//
//  CategorySelection.swift
//  RadiologyDesktop
//
//  Created by Alessandro Ferrazza on 21/08/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import MacBaseKit
import SharedFilesForMac

class CategorySelection: DataSourceList, DataSourceListProtocol {

    
    /*
    @IBAction func reloadData(sender:NSToolbarItem){
        self.reloadDataSource()
    }
    */

    
    var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.reloadDataSource()
        
        self.savingDirectoryForImage = "BaseData/CategoryAndGroupImages/CategoryImages/"

        
        if let split = self.parent as? MySplitController{
            self.splitViewController = split
            split.removeAllControllersFromStack()
            split.addViewControllerToStack(viewController: self)
        }
     
    }
    //MARK:Saving
    //2. SAVE THE FILE
    override func saveFileRemotely(with completion: @escaping (Error?) -> Void) {
        
        //Do not remove
        if self.dataSourceRemovedValues.count > 0{
            if let baseData = DataSourceRadiology.shared.categoriesBaseData{
                for value in self.dataSourceRemovedValues{
                    baseData.dictData.removeValue(forKey: value)
                }
                baseData.updateXMLElement()
                self.dataSourceRemovedValues.removeAll()
                
            }
        }
        
        if let newObject = self.dataSourceAdded.last{
            self.loadingLabel(loading: true, with: "Saving category with value: \(newObject.value)", color: nil)
            self.dataSourceAdded.removeLast()
            //save data
            if let baseData = DataSourceRadiology.shared.categoriesBaseData{
                baseData.setObject(dataSourceObjects: newObject, for: newObject.value)
            }
            
            //Create baseDataOtherSigns.xml e generalDataGroups.xml
            if self.selectedIndexPathToModify == nil{ //Is new category
                let categoryName = newObject.value
                let rootElement = MyXMLElement(name: "root")
                DispatchQueue.main.async {
                    self.loadingLabel(loading: true, with: "Creating baseDataOtherSigns.xml", color: nil)
                }
                DataSourceRadiology.saveXMLElementFile(element: rootElement, withFileName: "baseDataOtherSigns.xml", inDirectory: "DataSource/\(categoryName)/BaseData/", completion: { [weak self](success, result, error) in
                    //create file generalDataGroups.xml
                    DispatchQueue.main.async {
                        self?.loadingLabel(loading: true, with: "Creating generalDataGroups.xml", color: nil)
                    }
                    let rootElement = MyXMLElement(name: "root")
                    DataSourceRadiology.saveXMLElementFile(element: rootElement, withFileName: "generalDataGroups.xml", inDirectory: "DataSource/\(categoryName)/BaseData/", completion: { [weak self](success, result, error) in
                        self?.saveFileRemotely(with: completion)
                    })
                })
            }else{//is an old category
                self.saveFileRemotely(with: completion)
            }
            
            
        }else{
            if let xmlElement = DataSourceRadiology.shared.categoriesBaseData?.element{
                DataSourceRadiology.shared.saveCategoryXMLElement(element: xmlElement, completion: {(success, result, error) in
                    completion(error)
                    self.selectedIndexPathToModify = nil
                })
            }
        }
    }

    
    func reloadDataSource(){
        if isLoading == false{
            self.isLoading = true
            self.progressionIndicator?.isHidden = false

            self.loadingLabel(loading: true, with: "Downloading common base data.", color: nil)
            
            DataSourceRadiology.shared.loadBaseDataCommon(utilizeFileManager: false) { [weak self](error, element) in
                //print("called completion base data")
                self?.loadingLabel(loading: true, with: "Downloading categories.", color: nil)
                DataSourceRadiology.shared.loadCategories(utilizeFileManager: false, withCompletion: { [weak self] (error, element) in
                    //print("called completion loadCategories")

                    self?.loadingLabel(loading: true, with: "Downloading base data for category.", color: nil)
                    DataSourceRadiology.shared.loadBaseDataGroupsForCategory(utilizeFileManager: false, withCompletion: { (error, element) in
                        //print("called completion loadBaseDataGroupsForCategory")

                        DispatchQueue.main.async {
                            
                            self?.showUpdatedMessage(for: (element != nil) , error: error)
                            self?.loadDataFromBaseData()
                            self?.isLoading = false
                        }
                        
                    })
                    
                })
                
            }
            
        }
    }
    
    func loadDataFromBaseData(){
        if let dict = DataSourceRadiology.shared.categoriesBaseData?.dictData{
            self.dataSource = Array(dict.values)
            self.tableView.reloadData()

        }
        
    }
        
    //MARK: DataSourceListProtocoll
    
    func dataSourceListProtocolSelected(object: DataSourceObject, with dataSourceList: DataSourceList) {
        
        self.performSegue(withIdentifier: "CategorySegue", sender: self)
    }
    
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier{
            if identifier == "CategorySegue"{
                if let indexPath = self.indexPathForSelectedRow(){
                    let object = self.object(at: indexPath)
                    let category = object.value
                    DataSourceRadiology.shared._selectedCategory = category
                    if let nextScene = segue.destinationController as? GroupSelection{
                        nextScene.category = category
                        if let split = self.splitViewController as? MySplitController{
                            nextScene.splitViewController = split
                            split.addViewControllerToStack(viewController: nextScene)
                        }
                    }
                }
            }
        }
    }
    
    override func tableView(_ tableView: NSTableView, titleForHeaderInSection section: Int) -> String? {

        //Return somethings othrwise the action cell woll work not properly
        return "Categories"
    }
    
}
