//
//  ViewDisease.swift
//  i-Interstitium Translate
//
//  Created by Alessandro Ferrazza on 08/10/16.
//  Copyright © 2016 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import MacBaseKit
import SharedFilesForMac

class ViewDisease: ViewerSuperClass, ViewerSuperClassProtocol, SetupElementVCProtocol, SetupGeneralDataProtocol, ObjectMultiSelectionProtocol, ImageObjectVCProtocol, DataSourceListProtocol{

    var disease:Disease? = nil

    var selectedPopUpElementIndexPath:IndexPath? = nil

        
    func setDisease(disease:Disease?){
        self.unsavedChange = false
        self.disease = disease
        //self.diseaseTraslation = self.copyDiseaseForLanguage(disease: disease, language: self.translationLanguage)
        self.tableView.reloadData()
        self.loadingLabel(loading: false, with: "", color: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    //MARK: command bar and pop ups -> action implemented from superclass
    override func saveAction() {
        super.saveAction()
        self.saveDisease { (success, error) in
            //nothing
        }
    }
    override func translateAction(){
        super.translateAction()
        self.translateDisease()

    }
    
    override func addAction(){
        self.addSign()
        
    }
    var imageObjectsToRemove = [ImageObject]()
    override func removeAction(at indexPath: IndexPath) {
        if let disease = self.disease{
            var deleted = false
            
            let sectionComponent = self.sectionComponent(for: indexPath.section)
            switch sectionComponent {
            case SectionComponent.nameAndDescription:
                //do nothing
                break
            case SectionComponent.generalData:
                //do nothing
                break
            case SectionComponent.baseRadiologicSigns:
                //do nothing
                break
            case SectionComponent.images:
                let imageObj = disease.diseaseImages[indexPath.item]
                self.imageObjectsToRemove.append(imageObj)
                disease.diseaseImages.remove(at: indexPath.item)
                deleted = true
                
            case SectionComponent.signs:
                let result = self.signGroupNameAndElements(for: indexPath.section)
                let element = result.elements[indexPath.item]
                self.imageObjectsToRemove.append(contentsOf: element.images)
                disease.removeSign(with: element.name, inGroup: result.name)
                deleted = true

            case SectionComponent.otherData:
                disease.diseaseOtherData.remove(at: indexPath.item)
                self.disease?.saveXmlElementForActualLanguage()
                
                deleted = true

            case SectionComponent.references:
                disease.diseaseReferences.remove(at: indexPath.item)
                deleted = true

            }
            self.disease?.saveXmlElementForActualLanguage()
 
            deleted = true
            if deleted{
                self.unsavedChange = true
                
                self.removeRows(at: [indexPath], withAnimation: NSTableView.AnimationOptions.effectFade)
            }

            //self.tableView.reloadData()
        }
        
        
        self.selectedPopUpElementIndexPath = nil
    }
    func addSign(){
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let groupVC = storyboard.instantiateController(withIdentifier: "ListOfString") as! ListOfString
        let groupNames = DataSourceRadiology.shared.baseDataNames()
        groupVC.title = "Select the group of sign to utilize"
        groupVC.strings = groupNames
        groupVC.setupCompletion { [weak self] (group) in
            if let groupName = group{
                let storyboard = NSStoryboard(name: "Main", bundle: nil)
                let elementVC = storyboard.instantiateController(withIdentifier: "SetupElementVC") as! SetupElementVC
                elementVC.element = nil
                elementVC.delegate = self
                elementVC.dataSourceGroupName = groupName
                if let elements = self?.disease?.diseaseSigns[groupName]{
                    elementVC.otherElementsForRelation = elements
                }
                self?.dismiss(groupVC)
                self?.presentAsSheet(elementVC)

            }else{
                self?.selectedPopUpElementIndexPath = nil
            }
        }
        self.presentAsSheet(groupVC)
    }
    
    
    @IBAction func addImage(sender:NSToolbarItem){
        self.presentImageObjectVC(with: nil)
 
    }
    
    @IBAction func addOtherData(sender:NSToolbarItem){
        self.presentOtherDataSelection()
    }
    func presentImageObjectVC(with imageObject:ImageObject?){
        var arrayUrls:[URL]?

        if imageObject != nil{
            if let imgs = imageObject?.imageUrls{
                for url in imgs{
                    if let unwUrl = url{
                        if arrayUrls == nil{
                            arrayUrls = [URL]()
                        }
                        arrayUrls?.append(unwUrl)
                    }
                }
            }
        }else{
            //no images
        }
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateController(withIdentifier: "ImageObjectVC") as! ImageObjectVC
        //vc.arrayImageUrls = arrayUrls
        vc.utilizeFileManager = Settings.UTILIZE_FILE_MANAGER
        vc.imageObject = imageObject
        vc.delegate = self
        self.presentAsSheet(vc)

    }
    
    @IBAction func addBaseRadiologySign(sender:NSToolbarItem){
        self.presentBaseRadiologySignSelection()
        
    }
    
    func presentBaseRadiologySignSelection(){
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateController(withIdentifier: "ObjectMultiSelection") as! ObjectMultiSelection
        if let disease = self.disease{
            if let dictData = DataSourceRadiology.shared.baseRadiologySignsBaseData?.dictData{
                vc.dataSource = Array(dictData.values)
            }
            vc.categorized = true
            vc.setup(with: disease.diseasebaseRadiologySigns)
        }
        vc.delegateMultiselection = self
        self.presentAsSheet(vc)
    }
    func presentOtherDataSelection(){
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateController(withIdentifier: "ObjectSelection") as! ObjectSelection
        if let disease = self.disease{
            if let dictData = DataSourceRadiology.shared.otherDataBaseData?.dictData{
                var array = Array(dictData.values)
                var arrayIndexesToRemove = [Int]()
                for element in disease.diseaseOtherData{
                    if let index = DataSourceObject.arrayDataSourceObject(contain: array, objectWith: element.name){
                        arrayIndexesToRemove.append(index)
                    }
                }
                let _ = array.removeAtIndexes(arrayIndexesToRemove)
                
                vc.categorized = false
                vc.delegate = self
                if array.count > 0{
                    vc.dataSource = array
                    self.presentAsSheet(vc)

                }else{
                    self.loadingLabel(loading: false, with: "No more other signs to add", color: NSColor.red)
                }

            }
        }
    }
    
    func presentGeneralDataSelection(){
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateController(withIdentifier: "SetupGeneralData") as! SetupGeneralData
        if let disease = self.disease{
            vc.maleFemaleRatio = disease.diseaseMaleFemaleRatio
            vc.ageRanges = disease.diseaseAgeRanges
            if let onset = disease.diseaseOnset{
                if let onsetObj = DataSourceRadiology.shared.baseData(with: DataSourceRadiology.diseaseOnsetKey)?.dictData[onset]{
                    vc.onset = onsetObj
                }
            }
        }
        vc.delegate = self
        self.presentAsSheet(vc)
    }
    
    //MARK: Helper
    func copyDiseaseForLanguage(disease:Disease, language:String)->Disease{
        if let xmlElement = disease.xmlElement{
            let newDisease = Disease(xmlElement: xmlElement, identifier: disease.diseaseIdentifier!)
            newDisease.reloadElementForLanguage(language)
            return newDisease
        }
        return disease
        
    }
    
    //MARK: TableView helper
    enum SectionComponent {
        case nameAndDescription, generalData, signs, baseRadiologicSigns, images, otherData, references
    }
    
    func sectionComponent(for section: Int) -> SectionComponent{
        if let disease = self.disease{
            let numberOfSignsSections = disease.diseaseSigns.count
            switch section {
            case 0:
                return SectionComponent.nameAndDescription
            case 1:
                return SectionComponent.generalData
            case 2:
                return SectionComponent.baseRadiologicSigns
            case 2...(2 + numberOfSignsSections):
                return SectionComponent.signs
            case 3 + numberOfSignsSections:
                return SectionComponent.images
            case 4 + numberOfSignsSections:
                return SectionComponent.otherData
            default:
                return SectionComponent.references
            }
        }
        return SectionComponent.references
        
    }
    func signGroupNameAndElements(for section:Int)->(name:String, elements:[Element]){
        if let disease = self.disease{
            let names = disease.diseaseSigns.keys.sorted { (name0, name1) -> Bool in
                return name0.compare(name1) == ComparisonResult.orderedAscending
            }
            
            let index = section - 3
            var name = names[0]
            if index > 0{
                name = names[index]
            }
            let elements = disease.diseaseSigns[name]
            return (name:name, elements:elements!)
        }
        return (name:"", elements:[Element]())
    }
    
    
    //MARK: TableView Methods
    
    override func tableView(_ tableView: NSTableView, cellForRowAt indexPath: IndexPath, for tableColumn: NSTableColumn?) -> NSView? {
        if let utilizedDisease = self.disease{
            let language = Settings.language(for: tableColumn, in: tableView)
            utilizedDisease.reloadElementForLanguage(language)

            if tableColumn == tableView.tableColumns[0] || tableColumn == tableView.tableColumns[1]  {
                let value = self.textAndImage(for: utilizedDisease, indexPath: indexPath)
                
                if let cell = super.tableView(tableView, cellForRowAt: indexPath, for: tableColumn) as?
                    SubtitleTableCellView {

                    cell.textField?.stringValue = value.title
                    cell.textFieldSubtitle.stringValue = ""
                    if let d = value.attributedDetail{
                        cell.textFieldSubtitle.attributedStringValue = d
                    }else if let d = value.detail{
                        cell.textFieldSubtitle.attributedStringValue = NSMutableAttributedString(fromTaggedString: d, baseAttributes: [NSAttributedString.Key.font:NSFont.systemFont(ofSize: SubtitleTableCellView.detailFontSize), NSAttributedString.Key.foregroundColor:NSColor.controlTextColor])
                    }
                    
                    //Set the tag to know if the changed of the text come from textfield or textFieldSubtitle
                    cell.textField?.tag = ViewDisease.itaTfTagTitle
                    cell.textFieldSubtitle.tag = ViewDisease.itaTfTagDetail
                    if tableColumn == tableView.tableColumns[1]{//Traslate image
                        cell.textField?.tag = ViewDisease.translationTfTagTitle
                        cell.textFieldSubtitle.tag = ViewDisease.translationTfTagDetail
                    }
                    //set the delegate to trigger the method on changing text
                    cell.textField?.delegate = self
                    cell.textFieldSubtitle.delegate = self
                    //set editable for cell at section 0 (title) and 5 (images)
                    let sectionComponent = self.sectionComponent(for: indexPath.section)
                    if sectionComponent == SectionComponent.nameAndDescription || sectionComponent == SectionComponent.images{
                        cell.textField?.isEditable = true
                    }else{
                        cell.textField?.isEditable = false
                    }
                    if sectionComponent == SectionComponent.generalData || sectionComponent == SectionComponent.baseRadiologicSigns{
                        cell.textFieldSubtitle?.isEditable = false
                    }else{
                        cell.textFieldSubtitle?.isEditable = true
                    }
                    //Setting image cell with image object
                    cell.imageView?.image = #imageLiteral(resourceName: "trasparent")
                    if let image = value.imageObject?.image{
                        cell.imageView?.image = image
                    }else if let url = value.imageObject?.imageUrl{
                        if let imageCell = cell as? SubtitleImageTableCellView{
                            imageCell.progressIndicator.isIndeterminate = false
                            imageCell.progressIndicator.isHidden = false
                            imageCell.progressIndicator.startAnimation(cell)
                            
                            let downloader = ImageDownloader(with: url)
                            downloader.downloadImage(percentDownloadBlock: {(percentDownload) in
                                imageCell.progressIndicator.doubleValue = Double(percentDownload)
                            }, completionBlock: { (image, error, downloaded) in
                                if let img = image{
                                    DispatchQueue.main.async {
                                        imageCell.progressIndicator.stopAnimation(imageCell)
                                        imageCell.progressIndicator.isHidden = true
                                        imageCell.imageView?.image = img
                                    }
                                }
                            })
                        }
                    }
                    
                    return cell
                }
            }
        }
        return nil
    }
   
    override func tableView(_ tableView: NSTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var result = super.tableView(tableView, heightForRowAt: indexPath)//this made a setup in superview
        if let disease = self.disease{
            let value = self.textAndImage(for: disease, indexPath: indexPath)
            let widthColumn = self.tableView.tableColumns[0].width
            var imagePresent = false
            if value.imageObject != nil{
                imagePresent = true
            }
            result = SubtitleTableCellView.height(for: value.title, subTitle: value.detail, columnWidth: widthColumn, imagePresent: imagePresent)
            if result < 74 && imagePresent{ //imageview height
                result = 74
            }
            return result
        }
        return 74
    }
       
    override func tableView(_ tableView: NSTableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
    }
    
    
    
    override func tableView(_ tableView: NSTableView, didRightClickSelectRowAt indexPath: IndexPath, in rect: CGRect) {
        var element:Element?
        var imageObject:ImageObject?
        let sectionComponent = self.sectionComponent(for: indexPath.section)
        switch sectionComponent {
        case SectionComponent.images:
            
            if let imageObj = self.disease?.diseaseImages[indexPath.item]{
                self.selectedPopUpElementIndexPath = indexPath
                imageObject = imageObj
            }
            
        case SectionComponent.signs:
            let group = self.signGroupNameAndElements(for: indexPath.section)
            //let radiologicSigns = Element.arrayOfElementWithoutExcluded(group.elements).array
            let radiologicSigns = group.elements
            let sign = radiologicSigns[indexPath.item]
            self.selectedPopUpElementIndexPath = indexPath
            element = sign
        case SectionComponent.baseRadiologicSigns:
            self.presentBaseRadiologySignSelection()
        case SectionComponent.generalData:
            self.presentGeneralDataSelection()
        default:
            break
            //do nothing
        }
        
        
        if let e = element{
            let storyboard = NSStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateController(withIdentifier: "SetupElementVC") as! SetupElementVC
            vc.element = e
            vc.delegate = self
            let group = self.signGroupNameAndElements(for: indexPath.section)
            vc.dataSourceGroupName = group.name
            if let elements = self.disease?.diseaseSigns[group.name]{
                vc.otherElementsForRelation = elements
            }
            self.present(vc, asPopoverRelativeTo: rect, of: self.tableView, preferredEdge: NSRectEdge.minY, behavior: NSPopover.Behavior.semitransient)
        }
        if let i = imageObject{
            self.presentImageObjectVC(with: i)
        }
    }
    
    override func tableView(_ tableView: NSTableView, actionsForRowAtIndexPath indexPath: IndexPath, forEdge edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        let sectionComponent = self.sectionComponent(for: indexPath.section)
        if sectionComponent != SectionComponent.nameAndDescription && sectionComponent != SectionComponent.generalData{
            return super.tableView(tableView, actionsForRowAtIndexPath: indexPath, forEdge: edge)
        }
        
        return [NSTableViewRowAction]()
    }
    override func tableView(_ tableView: NSTableView, numberOfRowsInSection section: Int) -> Int{
        if let disease = self.disease{
            let sectionComponent = self.sectionComponent(for: section)
            switch sectionComponent {
            case SectionComponent.nameAndDescription:
                return 1
            case SectionComponent.generalData:
                return 1
            case SectionComponent.baseRadiologicSigns:
                let dict = self.dictForBaseRadiologySigns(for:disease)
                return dict.count
            case SectionComponent.images:
                return disease.diseaseImages.count
            case SectionComponent.signs:
                let group = self.signGroupNameAndElements(for: section)
                //let number = Element.arrayOfElementWithoutExcluded(group.elements).array.count
                let number = group.elements.count
                return number
            case SectionComponent.otherData:
                return disease.diseaseOtherData.count
            case SectionComponent.references:
                return disease.diseaseReferences.count
            }
        }
        return 0
        
    }
    
    override func numberOfSections(in tableView: NSTableView) -> Int {
        if let d = self.disease{
            return 5 + d.diseaseSigns.count
        }
        return 0
    }
    
    override func tableView(_ tableView: NSTableView, titleForHeaderInSection section: Int) -> String? {
        //if let disease = self.disease{
        let sectionComponent = self.sectionComponent(for: section)
        
        switch sectionComponent {
        case SectionComponent.nameAndDescription:
            return "Name and Description".localized
        case SectionComponent.generalData:
            return "General Data".localized
        case SectionComponent.baseRadiologicSigns:
            return "Base Radiologic Signs".localized
        case SectionComponent.images:
            return "Images".localized
        case SectionComponent.signs:
            let group = self.signGroupNameAndElements(for: section)
            //let signs = Element.arrayOfElementWithoutExcluded(group.elements).array
            let signs = group.elements
            if signs.count > 0{
                return group.name.localized
            }
        case SectionComponent.otherData:
            return "Other Data".localized
        case SectionComponent.references:
            return "Reference".localized
        }
        
        return nil
        
    }
    
    
    
    //MARK: Data-View Adapter
    
    func titleAndElements(for dict:[String:[Element]], at row:Int)->(category: String, elements:[Element]){
        let sortedKeys = dict.keys.sorted()
        let category = sortedKeys[row]
        return (category, dict[category]!)
    }
    
    func dictForBaseRadiologySigns(for disease:Disease)->[String:[Element]]{
        var result = [String:[Element]]()
        if let baseData = DataSourceRadiology.shared.baseRadiologySignsBaseData{
            result = Element.dictElementForCategory(from: disease.diseasebaseRadiologySigns, baseData: baseData)
        }
        return result
    }
    
    override func imageObject(at indexPath: IndexPath) -> ImageObject? {
        if let disease = self.disease{
            let value = self.textAndImage(for: disease, indexPath: indexPath)
            return value.imageObject
        }
        return nil
    }
    
    override func description(at indexPath: IndexPath) -> String? {
        if let disease = self.disease{
            let value = self.textAndImage(for: disease, indexPath: indexPath)
            return value.detail
        }
        return nil
    }
    
    
    
    func textAndImage(for disease:Disease, indexPath:IndexPath)->(title:String, detail:String?, attributedDetail:NSAttributedString?, imageObject:ImageObject?){
        var title = ""
        var detail:String?
        var attributedDetail:NSAttributedString?
        var imageObject:ImageObject?
        let sectionComponent = self.sectionComponent(for: indexPath.section)

        
        switch sectionComponent {
        case SectionComponent.nameAndDescription:
            if let name = self.disease?.diseaseName{
                title = name
            }
            detail = self.disease?.diseaseDescription
            
        case SectionComponent.baseRadiologicSigns:
            //get dict from elements
            let dict = self.dictForBaseRadiologySigns(for:disease)
            let tuple = self.titleAndElements(for: dict, at: indexPath.item)
            title = tuple.category
            detail = ""
            
            let sortedElements = tuple.elements.sorted(by: { (e1, e2) -> Bool in
                return e1.name.compare(e2.name) == ComparisonResult.orderedAscending
            })
            var arrayOfArrayOfImages = [[NSImage]]()
            var actualPhase:String?
            var combinedDescription = ""
            var firstDescriptionAdded = false
            for (index, e) in sortedElements.enumerated(){
                //adding descriptions
                if let d = e.elementDescription{
                    if firstDescriptionAdded{
                        combinedDescription = combinedDescription + "; " + d
                    }else{
                        combinedDescription = d
                    }
                    firstDescriptionAdded = true
                }
                //get obj
                if let obj = DataSourceRadiology.shared.baseRadiologySignsBaseData?.object(with: e.name){
                    //check if actualPhase change
                    var actualPhaseChange = false
                    let components = obj.value.components(separatedBy: "_")
                    if components.count > 1{
                        let phaseIndex = 0
                        if let actual = actualPhase{
                            if actual != components[phaseIndex]{
                                actualPhaseChange = true
                                actualPhase = components[phaseIndex]
                            }
                        }else{
                            actualPhaseChange = true
                            actualPhase = components[phaseIndex]
                        }
                    }else{
                        actualPhaseChange = true
                    }
                    
                    
                    
                    if index == 0{
                        detail = " - " + obj.name
                    }else{
                        if actualPhaseChange{
                            detail = detail! + "\n - "
                        }else{
                            detail = detail! + ", "
                        }
                        detail = detail! + obj.name
                    }
                    if let image = obj.imageObject?.image{
                        if actualPhaseChange{
                            arrayOfArrayOfImages.append([image])
                        }else{
                            if var last = arrayOfArrayOfImages.last{
                                last.append(image)
                                arrayOfArrayOfImages[arrayOfArrayOfImages.count - 1] = last
                            }
                        }
                    }
                }
            }
            var italicFont = NSFont.systemFont(ofSize: 14)
            italicFont = NSFontManager.shared.convert(italicFont, toHaveTrait: NSFontTraitMask.italicFontMask)
            let attributed = detail?.mutableAttributed(with: NSColor.controlTextColor, font:italicFont )
            if combinedDescription.count > 0{
                
                
            }
            attributedDetail = attributed
            if combinedDescription.count > 0{
                let toAppend = "\n" + combinedDescription
                detail = detail! + toAppend
                attributed?.append(toAppend.attributed(with: NSColor.controlTextColor, font: NSFont.systemFont(ofSize: 14)))
            }
            attributedDetail = attributed
            if arrayOfArrayOfImages.count > 0{
                let combinedImage = DrawHelper.drawImagesInSlices(images: arrayOfArrayOfImages, size: 100)
                imageObject = ImageObject(image: combinedImage)
            }
        
        case SectionComponent.generalData:
            if let name = self.disease?.diseaseName{
                title = name
            }
            detail = CorrespondanceCalculator.attributedString(for: disease, fontSize:  16, addOtherSigns:false, addClinicalSigns: true).string
            if let onset = disease.diseaseOnset{
                if let onsetObj = DataSourceRadiology.shared.baseData(with: DataSourceRadiology.diseaseOnsetKey)?.dictData[onset]{
                    title = onsetObj.name(for: disease.actualLanguage)
                    imageObject = onsetObj.imageObject
                }
            }else{
                title = "Onset not set"
                imageObject = ImageObject(image: #imageLiteral(resourceName: "unsetOnset"))
            }
            
        case SectionComponent.images:
            let imageObj = disease.diseaseImages[indexPath.item]
            title = imageObj.imageTitle
            detail = imageObj.imageDescription
        
            imageObject = imageObj
            
        case SectionComponent.signs:
            let result = self.signGroupNameAndElements(for: indexPath.section)
            
            let element = result.elements[indexPath.item]
            let signName =  element.name
            
            
            let dictData = DataSourceRadiology.shared.baseData(with: result.name)?.dictData
            title = element.getNameAndDataString(from: dictData)
            
            detail = element.elementDescription
            if let signObj = dictData?[signName]{
                
                imageObject = signObj.imageObject
                if let imgName = signObj.imageName{
                    if let image = NSImage(named: imgName){
                        imageObject = ImageObject(image: image)
                    }
                }
            }
            if element.images.count > 0{
                imageObject = element.images[0]
            }
        case SectionComponent.otherData://es. clinical, perfusion ecc
            let element = disease.diseaseOtherData[indexPath.item]
            let signName =  element.name
            
            let dictData = DataSourceRadiology.shared.otherDataBaseData?.dictData
            title = signName
            detail = element.elementDescription
            if let signObj = dictData?[signName]{
                imageObject = signObj.imageObject
                if let imgName = signObj.imageName{
                    if let image = NSImage(named: imgName){
                        imageObject = ImageObject(image: image)
                    }
                }
            }
            
        case SectionComponent.references:
            let ref = disease.diseaseReferences[indexPath.item]
            title = ref.title
            detail = "\(String(describing: ref.authors))\n\(String(describing: ref.journal))"
        }
        
        return (title, detail, attributedDetail, imageObject)
    }
    
    
    //MARK: ViewerSuperClassProtocol

    override func save(titleText:String, at indexPath:IndexPath, for language:String){
        self.unsavedChange = true
        if let disease = self.disease{
            disease.reloadElementForLanguage(language)
            let sectionComponent = self.sectionComponent(for: indexPath.section)
            
            switch sectionComponent {
            case SectionComponent.nameAndDescription:
                disease.diseaseName = titleText
            case SectionComponent.images:
                let image = disease.diseaseImages[indexPath.item]
                image.imageTitle = titleText
            default:
                break
            }
            disease.saveXmlElementForActualLanguage()
        }
    }
    override func save(detailText:String, at indexPath:IndexPath, for language:String){
        
        self.unsavedChange = true
        if let disease = self.disease{
            disease.reloadElementForLanguage(language)
            
            let sectionComponent = self.sectionComponent(for: indexPath.section)
            
            switch sectionComponent {
            case SectionComponent.nameAndDescription:
                disease.diseaseDescription = detailText
            case SectionComponent.images:
                let image = disease.diseaseImages[indexPath.item]
                image.imageDescription = detailText
            case SectionComponent.signs:
                let group = self.signGroupNameAndElements(for: indexPath.section)
                //let radiologicSigns = Element.arrayOfElementWithoutExcluded(group.elements).array
                let radiologicSigns = group.elements
                let element = radiologicSigns[indexPath.item]
                element.elementDescription = detailText
                if disease.replaceSign(signToReplace: element, inGroup: group.name) == false{
                    print("Error in replacing sign with name \(element.name)")
                }
                
            case SectionComponent.otherData:
                let element = disease.diseaseOtherData[indexPath.item]
                element.elementDescription = detailText
              
            default:
                break
            }
            disease.saveXmlElementForActualLanguage()
           
        }
        
        
    }
    //MARK: Saving

    func checkIfDiseaseIsValid()->Bool{
        if let disease = self.disease{
            if disease.diseaseName.count > 0{
                if disease.xmlElement == nil{
                    disease.newXmlElementForActualLanguage()
                }
                if disease.diseaseIdentifier == nil{//
                    //Create new file self.disease.diseaseName
                }
                return true
            }
        }
        return false
    }
    func baseDirectoryToSaveImages()->String?{
        var baseDirectory:String?
        if let cat = self.category{
            baseDirectory = "DataSource/" + cat + "/"
        }
        return baseDirectory
    }
    func saveDisease(_ completion: @escaping (_ success: Bool, _ error: Error?) -> Void){
        
        if self.imageObjectsToRemove.count > 0{
            let imageObject = self.imageObjectsToRemove.removeLast()
            ImageManager.shared.deleteImageObject(imageObject: imageObject, withCompletionBlock: {[weak self] (data, response, error) in
                self?.saveDisease(completion)
            })
            return
        }
        
        let errorColor = NSColor(calibratedRed: 0.5, green: 0, blue: 0, alpha: 1)
        let savingColor = NSColor(calibratedRed: 0, green: 0.5, blue: 0, alpha: 1)
        
        
        if self.checkIfDiseaseIsValid(){
            DispatchQueue.main.async {[weak self]() in
                self?.loadingLabel(loading: true, with: "Saving disease...", color:nil)
                self?.tableView.window?.makeFirstResponder(nil)//end editing
            }
            
           
            if let disease = self.disease, let category = self.category, let group = self.group{
                if (disease.diseaseIdentifier == nil){//new disease never saved
                    //make a first save to get the disease identifier
                    disease.save(in: group, category: category, completion: { [weak self](success, error) in
                        DispatchQueue.main.async {
                            self?.showSaveMessage(for: success, error: error)
                            if success{
                                self?.unsavedChange = false
                                self?.saveDisease(completion)
                            }else{
                                self?.loadingLabel(loading: false, with: "Error during saving: \(String(describing: error))", color: errorColor)
                            }
                        }
                        
                    })
                    
                }else{//old disease previously saved
                    self.loadingLabel(loading: true, with: "Loading images...", color: savingColor)
                    ImageManager.shared.saveImageObjects(withDiseaseId: disease.diseaseIdentifier,
                                                         objectName: nil,
                                                         baseDirectory: self.baseDirectoryToSaveImages(),
                                                         imageObjects: disease.diseaseImages,
                                                         percentUploadBlock: { [weak self](percentUpload) in
                                                            DispatchQueue.main.async {
                                                                self?.setPercentProgressionIndicator(percent: percentUpload)
                                                            }},
                                                         singleImageLoadingCompletion: { [weak self](imageNumber, sequenceCount) in
                                                            DispatchQueue.main.async {
                                                                let message = "image sequence".localized + " \(imageNumber)/\(sequenceCount)"
                                                                self?.loadingLabel(loading: true, with: message, color: savingColor)
                                                            }},
                                                         singleImageObjectLoadingCompletion: {[weak self](imageNumber, imageCount) in
                                                            DispatchQueue.main.async {
                                                                let message = "image".localized + " \(imageNumber)/\(imageCount)"
                                                                self?.loadingLabel(loading: true, with: message, color: savingColor)
                                                            }},
                                                         finalCompletion: { [weak self](resultImageObjects) in
                                                            DispatchQueue.main.async {
                                                                //upload element
                                                                self?.loadingLabel(loading: true, with: "Saving file", color: savingColor)
                                                                self?.disease?.diseaseImages = resultImageObjects
                                                                self?.disease?.save(in: group, category: category, completion: {[weak self](success, error) in
                                                                    self?.unsavedChange = false
                                                                    self?.showSaveMessage(for: success, error: error)
                                                                    completion(success, error)
                                                                    
                                                                    self?.sendSaveNotification()
                                                                    
                                                                })
                                                            
                                                            }
                                                    
                    })
                }
            }
        }

    }
    

    //MARK: SetupGeneralDataProtocol
    
    func setupGeneralDataProtocolDidEnd(with setupGeneralData: SetupGeneralData?) {
        self.disease?.diseaseMaleFemaleRatio = setupGeneralData?.maleFemaleRatio
        self.disease?.diseaseAgeRanges = (setupGeneralData?.ageRanges)!
        self.disease?.diseaseOnset = setupGeneralData?.onset?.value
        self.disease?.saveXmlElementForActualLanguage()
        if let ip = self.selectedIndexPath{
            self.reloadRows(at: [ip], forColumns: [0, 1])
        }
        self.selectedIndexPath = nil
        self.unsavedChange = true
        self.dismiss(setupGeneralData!)
        self.tableView.reloadData()


    }
    func setupGeneralDataProtocolDidCancel(with setupGeneralData: SetupGeneralData?) {
        self.selectedIndexPath = nil
        self.dismiss(setupGeneralData!)

    }

    //MARK: DataSourceListProrocol
    //For selection of other data
    func dataSourceListProtocolSelected(object: DataSourceObject, with dataSourceList: DataSourceList) {
        self.dismiss(dataSourceList)
        self.unsavedChange = true
        let element = Element(name: object.value, description: nil, imageUrls: nil)
        self.disease?.diseaseOtherData.append(element)
        self.disease?.saveXmlElementForActualLanguage()
        self.tableView.reloadData()
    }
    //MARK: objectMultiSelectionProtocol
    func objectMultiSelectionDidEnd(with arrayOfElement: [Element], objectMultiSelection: ObjectMultiSelection) {
        self.dismiss(objectMultiSelection)
        self.unsavedChange = true
        self.disease?.diseasebaseRadiologySigns = arrayOfElement
        self.disease?.saveXmlElementForActualLanguage()
        self.tableView.reloadData()
    }
    
    func objectMultiSelectionDidCancel(objectMultiSelection: ObjectMultiSelection) {
        self.dismiss(objectMultiSelection)
    }
    
    //MARK: SetupElementVCProtocol
    func setupElementVCProtocolModified(groupName:String?, element: Element, with setupElementVC: SetupElementVC) {
        self.unsavedChange = true
        self.disease?.saveXmlElementForActualLanguage()
        if let ip = self.selectedPopUpElementIndexPath{
            self.reloadRows(at: [ip], forColumns: [0,1])
        }
        self.selectedPopUpElementIndexPath = nil
    }
    func setupElementVCProtocolAdd(groupName:String?, element: Element, with setupElementVC: SetupElementVC) {
        
        self.unsavedChange = true
        if let name = groupName{
            if var group = self.disease?.diseaseSigns[name]{
                group.append(element)
                self.disease?.diseaseSigns[name] = group
            }else{
                self.disease?.diseaseSigns[name] = [element]

            }
            self.disease?.saveXmlElementForActualLanguage()

            self.tableView.reloadData()
            
        }
        self.selectedPopUpElementIndexPath = nil
 
    }
    func setupElementVCProtocolDeleteSelected(groupName:String?, with setupElementVC: SetupElementVC) {
        
        self.unsavedChange = true
        if let name = groupName{
            if var group = self.disease?.diseaseSigns[name]{
                if let ip = self.selectedPopUpElementIndexPath{
                    group.remove(at: ip.item)
                }
                self.disease?.diseaseSigns[name] = group
            }
            self.disease?.saveXmlElementForActualLanguage()
            
            if let ip = self.selectedPopUpElementIndexPath{
                self.removeRows(at: [ip], withAnimation: NSTableView.AnimationOptions.effectFade)
            }
        }
        self.selectedPopUpElementIndexPath = nil
        
    }
    
    //MARK: ImageObjectVCProtocol
    func imageObjectVCProtocolDidEnd(with arrayImages: [NSImage]?, imageObjectVC: ImageObjectVC) {
        if let newImages = arrayImages, let disease = self.disease{
            let imageObj = ImageObject(images: newImages)
            imageObj.imageTitle = disease.diseaseName
            if let ip = self.selectedPopUpElementIndexPath{
                let oldImageObj = disease.diseaseImages[ip.item]
                imageObj.imageDescription = oldImageObj.imageDescription
                imageObj.imageTitle = oldImageObj.imageTitle
                disease.diseaseImages[ip.item] = imageObj

            }else{
                self.disease?.diseaseImages.append(imageObj)
            }
            self.unsavedChange = true
            self.disease?.saveXmlElementForActualLanguage()
            if let ip = self.selectedPopUpElementIndexPath{
                self.reloadRows(at: [ip], forColumns: [0,1])
            }else{
                self.tableView.reloadData()
            }
        }
        self.selectedPopUpElementIndexPath = nil
    }

    func imageObjectVCProtocolCancel(imageObjectVC: ImageObjectVC) {
        self.selectedPopUpElementIndexPath = nil
    }

    //MARK:Translating
    func translateDisease(){
        if let ip = self.selectedIndexPath, let disease = self.disease{
            self.loadingLabel(loading: true, with: "Traslating...", color: nil)
            disease.reloadElementForLanguage("ita")
            let value = self.textAndImage(for: disease, indexPath: ip)
            
            if let detail = value.detail{
                
                GoogleTranslateManager.translateToEnglish(text: detail, completionHandler: { [weak self] (text, error) in
                    var success = true
                    if error != nil{
                        success = false
                    }
                    self?.showTranslateMessage(for: success, error: error)
                    if let tableView = self?.tableView{
                        if let row = self?.rowNumber(for: ip, in: tableView), let t = text, let columnId = self?.tableView.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue:"TranslationColumnId")){
                            if let translationLanguage = self?.translationLanguage{
                                let correctedText = MyXMLNode.cleanedStringFromXML(string: t)
                                self?.save(text: correctedText!, at: ip, for: ViewDisease.translationTfTagDetail)
                                self?.disease?.reloadElementForLanguage(translationLanguage)
                                
                                
                                //self?.tableView.reloadData()
                                self?.tableView.reloadData(forRowIndexes: IndexSet.init(integer:row), columnIndexes: IndexSet.init(integer: columnId))
                                self?.selectedIndexPath = nil
                            }
                        }
                    }
                    
                })
            }else{
                self.loadingLabel(loading: false, with: "Nothing to translate", color: NSColor.gray)
            }
            
        }
    }

}
