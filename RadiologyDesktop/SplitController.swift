//
//  SplitController.swift
//  i-Interstitium Translate
//
//  Created by Alessandro Ferrazza on 08/10/16.
//  Copyright © 2016 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import SharedFilesForMac

class MySplitController: SplitController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do view setup here.
    }
    
    private var stackLeftViewController = [NSViewController]()
    
    func removeAllControllersFromStack(){
        self.stackLeftViewController.removeAll()
    }
    func addViewControllerToStack(viewController:NSViewController){
        self.stackLeftViewController.append(viewController)
        //print("add \(self.stackLeftViewController.count)")
    }
    func backToPreviousViewController(){
        if self.stackLeftViewController.count > 1{
            let dismissingViewController = self.stackLeftViewController.last
            self.stackLeftViewController.removeLast()
            let dismisserViewController = self.stackLeftViewController.last
            dismisserViewController!.dismiss(dismissingViewController!)
            if let tableVC = dismisserViewController as? DataSourceList{
                tableVC.tableView.deselectAll(self)
            }        
        }
    }
    
    func backToRootViewController(){
        if self.stackLeftViewController.count > 1{
            let rootViewController = self.stackLeftViewController[0]
            let secondViewController = self.stackLeftViewController[1]
            self.stackLeftViewController.removeAll()
            self.stackLeftViewController.append(rootViewController)
            rootViewController.dismiss(secondViewController)
            if let tableVC = rootViewController as? CategorySelection{
                tableVC.tableView.deselectAll(self)
                tableVC.reloadDataSource()
            }
            
        }
        //print("remove \(self.stackLeftViewController.count)")
        
    }
    
    func replace(with toViewController:NSViewController, at index:Int){
        //let item = NSSplitViewItem(viewController: toViewController)
        //self.insertSplitViewItem(item, at: index)
        self.children[index] = toViewController

    }

    
}
