//
//  ListOfDiseases.swift
//  i-Interstitium Translate
//
//  Created by Alessandro Ferrazza on 08/10/16.
//  Copyright © 2016 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import MacBaseKit
import SharedFilesForMac

class ListOfDiseases: LeftSplitViewController, ListOfStringProtocol {

    var diseaseArray = [Disease]()
    var generalDataSource = [DataSourceObjectCollection]()

    var dictNameImage = [String:NSImage]()//A dict to remember the image downloaded and avoid to recall them every time the cell will scroll


    @IBAction func reloadData(sender:NSToolbarItem){
        self.reloadData()
    }
    
    @IBAction func callSearchForm(sender:NSToolbarItem){
        print("Search - to implement")
    }

    @IBAction func addCollectionAction(sender:NSButton){
        
        let arrayStrings = DataSourceRadiology.shared.arrayGeneralDataNames
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateController(withIdentifier: "ListOfString") as! ListOfString
        vc.strings = arrayStrings
        vc.delegate = self
        self.presentAsSheet(vc)
    }
    
    func listOfStringDidEnd(with string: String, listOfString: ListOfString) {
        self.dismiss(listOfString)
        for (index, collection) in self.generalDataSource.enumerated() {
            if collection.name == string{
                self.showCollection(at: index, withNewName: nil)
                return
            }
        }
        self.showCollection(at: nil, withNewName: string)
    }
    func listOfStringDidCancel(listOfString: ListOfString) {
        //nothing
    }
    
    @IBAction func addDiseaseAction(sender:NSButton){
        self.showDisease(at: nil)
    }
    
    
    
    var group:String?
    var category:String?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: ViewerSuperClass.saveNotificationNameKey), object: nil, queue: nil) { [weak self](notification) in
            self?.reloadData()
        }
        self.reloadData()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewDidAppear() {
        super.viewDidAppear()
    }
    
    func reloadData(){
        if self.checkForUnsavedChanges() == false{
            self.progressionIndicator?.isHidden = false
            if let category = self.category, let group = self.group{
                let utilizeFileManager = false
                self.loadingLabel(loading: true, with: "Loading data...", color:nil)
                DataSourceRadiology.loadDataSource(forCategory: category, group: group, utilizeFileManager: utilizeFileManager, completionFile: { (error, fileName) in
                    if let name = fileName{
                        self.loadingLabel(loading: true, with: "Downloading diseases: \(name)", color: nil)
                    }
                    
                }) { [weak self] (error, dataSourceDict) in
                    DispatchQueue.main.async {
                        
                        if error != nil{
                            self?.showUpdatedMessage(for: false, error: error)
                        }else{
                            self?.showUpdatedMessage(for: true, error: nil)
                            if let generalDataGroup = DataSourceRadiology.shared.dictGeneralDataGroups[group]{
                                self?.generalDataSource = generalDataGroup
                            }
                            self?.diseaseArray = DataSourceRadiology.listOfDiseases(from: dataSourceDict)
                            self?.tableView.reloadData()
                        }
                    }
                    
                }
            }
        }
    }
    
    
    override func tableView(_ tableView: NSTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var text = ""
        if indexPath.section == 0{
            let elementGroup = self.generalDataSource[indexPath.item]
            text = elementGroup.name
            
        }else{
            let disease = self.diseaseArray[indexPath.item]
            text = disease.diseaseName
        }
        let column = tableView.tableColumns[0]
        let widthColumn = column.width
        let imagePresent = true
        var result = SubtitleTableCellView.height(for: text, subTitle: nil, columnWidth: widthColumn, imagePresent: imagePresent)
        if result < 74 && imagePresent{ //imageview height
            result = 74
        }
        return result
    }
    
    override func numberOfSections(in tableView: NSTableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: NSTableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return self.generalDataSource.count
        }else{
            return self.diseaseArray.count
        }
    }

    override func tableView(_ tableView: NSTableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "General Info"
        }else{
            return "Diseases"
        }
    }
    override func tableView(_ tableView: NSTableView, cellForRowAt indexPath: IndexPath, for tableColumn: NSTableColumn?) -> NSView? {
        let cellIdentifier = "subtitleTableCellView"
        let language = Settings.language(for: tableColumn, in: tableView)
        if let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: cellIdentifier), owner: nil) as? SubtitleTableCellView {

            var text = ""
            var description = ""
            var imageObject:ImageObject?
            cell.imageView?.image = nil
            cell.textFieldSubtitle.textColor = NSColor(calibratedRed: 0.5, green: 0.5, blue: 0.5, alpha: 1)
            
            if indexPath.section == 0{//general data
                let elementGroup = self.generalDataSource[indexPath.item]
                text = elementGroup.nameAndDescriptionObj.name
                if let d = elementGroup.nameAndDescriptionObj.description(for: language){
                    description = d
                }

                imageObject = elementGroup.imageObject
                
            }else{//diseases
                let disease = self.diseaseArray[indexPath.item]
                
                text = disease.diseaseName
                
                if let d = disease.diseaseDescription{
                    description = d
                }
                if disease.diseaseImages.count > 0{
                    imageObject = disease.diseaseImages[0]
                }
                
                
            }
            
            cell.textField?.stringValue = text.localized
            cell.textFieldSubtitle.stringValue = description
            if let image = self.dictNameImage[text]{
                cell.imageView?.image = image
            }else if let imageObj = imageObject{
                if let image = imageObj.image{
                    cell.imageView?.image = image
                }else if let url = imageObj.imageUrl{
                    if let imageCell = cell as? SubtitleImageTableCellView{
                        imageCell.progressIndicator.isIndeterminate = false
                        imageCell.progressIndicator.isHidden = false
                        imageCell.progressIndicator.startAnimation(cell)
                        //let session = URLSession.shared
                        let downloader = ImageDownloader(with: url)
                        downloader.downloadImage(percentDownloadBlock: {(percentDownload) in
                            DispatchQueue.main.async {
                                imageCell.progressIndicator.doubleValue = Double(percentDownload)
                            }
                        }, completionBlock: { [weak self](image, error, downloaded) in
                            if let img = image{
                                DispatchQueue.main.async {
                                    imageCell.progressIndicator.stopAnimation(imageCell)
                                    imageCell.progressIndicator.isHidden = true
                                    self?.dictNameImage[text] = img
                                    imageCell.imageView?.image = img
                                }
                            }
                        })
                    }
                }
            }
            
            return cell
            
        }
        return nil
    }
        
    override func tableView(_ tableView: NSTableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            self.showCollection(at: indexPath.item, withNewName: nil)
        }else{
            self.showDisease(at: indexPath.item)

        }
    }
    override func tableView(_ tableView: NSTableView, actionsForRowAtIndexPath indexPath: IndexPath, forEdge edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        var actions = [NSTableViewRowAction]()
        if edge == NSTableView.RowActionEdge.trailing{
            let action = NSTableViewRowAction(style: NSTableViewRowAction.Style.destructive, title: "Delete") { [weak self](action, row) in
                if indexPath.section == 0{//collection
                    self?.removeCollection(at: indexPath.item)
                  
                }else{//disease
                    self?.removeDisease(at: indexPath.item)
                    
                }
                self?.removeRows(at: [indexPath], withAnimation: NSTableView.AnimationOptions.effectFade)

                
            }
            action.image = NSImage(named: NSImage.trashEmptyName)
            actions.append(action)
        }
        return actions
    }
    func removeCollection(at index:Int){
        let collection = self.generalDataSource[index]
        if let category = self.category, let group = self.group{
            DataSourceRadiology.shared.deleteGeneralDataCollection(with: collection,
                                                                   inCategory: category,
                                                                   groupName: group,
                                                                   withCompletionImageObjectRemoved: { [weak self](result) in
                                                                    DispatchQueue.main.async {
                                                                        if let r = result{
                                                                            self?.loadingLabel(loading: true, with: r, color: nil)
                                                                        }
                                                                    }},
                                                                   completion: { [weak self](success, error) in
                                                                    DispatchQueue.main.async {
                                                                        self?.showSaveMessage(for: success, error: error)
                                                                    }
            })
            self.generalDataSource.remove(at: index)
        }
    }
    func removeDisease(at index:Int){
        let disease = self.diseaseArray[index]
        if let category = self.category{
            DataSourceRadiology.shared.deleteRemotely(disease: disease, in: category,
                                                      withCompletionImageObjectRemoved: { [weak self](result) in
                                                        DispatchQueue.main.async {
                                                            if let r = result{
                                                                self?.loadingLabel(loading: true, with: r, color: nil)
                                                            }
                                                        }},
                                                      withCompletionDirectoryImagesRemoved: { [weak self](result) in
                                                        DispatchQueue.main.async {
                                                            if let r = result{
                                                                self?.loadingLabel(loading: true, with: r, color: nil)
                                                            }
                                                        }},
                                                      withCompletionDiseaseRemoved: { [weak self](result) in
                                                        DispatchQueue.main.async {
                                                            if let r = result{
                                                                self?.loadingLabel(loading: false, with: r, color: NSColor.myDarkGreen)
                                                            }
                                                        }
            })
            self.diseaseArray.remove(at: index)
        }
    }

    func showCollection(at index:Int?, withNewName:String?){
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        var objectCollection: DataSourceObjectCollection
        if let i = index{
            objectCollection = self.generalDataSource[i]
        }else{
            var name = "New"
            if let newName = withNewName{
                name = newName
            }
            objectCollection = DataSourceObjectCollection(name: name)
        }
        
        if self.checkForUnsavedChanges() == false{
            let objectCollectionVC = storyboard.instantiateController(withIdentifier: "ViewObjectCollection") as! ViewObjectCollection
            objectCollectionVC.group = self.group
            objectCollectionVC.objectCollection = objectCollection
            if let split = self.splitViewController as? MySplitController{
                split.replace(with: objectCollectionVC, at: 1)

            }
        }

    }
    
    func showDisease(at index:Int?){
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        var disease: Disease
        if let i = index{
            disease = self.diseaseArray[i]
        }else{
            disease = Disease(language: "ita")
        }
        if self.checkForUnsavedChanges() == false{
            disease.reloadElementForLanguage("ita")
            let viewDiseaseVC = storyboard.instantiateController(withIdentifier: "ViewDisease") as! ViewDisease
            
            viewDiseaseVC.group = self.group
            viewDiseaseVC.category = self.category
            viewDiseaseVC.disease = disease
            if let split = self.splitViewController as? MySplitController{
                split.replace(with: viewDiseaseVC, at: 1)

            }
        }
    }
    
    //MARK: HELPER

    func checkForUnsavedChanges()->Bool{
        if let split = self.splitViewController{
            if let vc = split.children[1] as? ViewerSuperClass{
                if vc.unsavedChange == true{
                    if let response = vc.showAlertSaveUnchanged(){
                        if response == false{
                            vc.unsavedChange = true
                            return false
                        }else{
                            if let vcDisease = vc as? ViewDisease{
                                vcDisease.saveDisease({ (success, error) in
                                    //Do nothing
                                })
                            }
                            if let vcCollection = vc as? ViewObjectCollection{
                                vcCollection.save()
                            }
                            
                            return true
                        }
                    }else{//pressed cancel
                        return true
                    }
                }else{
                    return false
                }
            }
 
        }
        return false
    }
    
    
}
