//
//  DataSourceRadiology.swift
//  Radiology
//
//  Created by Alessandro Ferrazza on 07/06/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//



import MacBaseKit
import SharedFilesForMac


class DataSourceRadiology: DataSourceProtocol, ImageObjectProtocol, ImageManagerProtocol {
    
    
    
    //singleton
    //create this static property in subclass
    static let shared = DataSourceRadiology()
    init() {
        //Initial Setting
        ImageManager.shared.imageObjectDelegate = self
        DataSource.shared.delegate = self
        ImageManager.shared.delegate = self
        
    }
    
    let imageDirectory = ""
    
    let deletePhpDisease = URL(string: "http://46.22.208.75/apps/Radiology/deleteDiseaseWithId.php")
    let deletePhpFile = URL(string: "http://46.22.208.75/apps/Radiology/deleteFileWithUrl.php")
    var _selectedCategory:String?

    
    static let language = "language"
    static let commonClinicalSignsKey = "commonClinicalSigns"


    //MARK: ImageObjectProtocol
    func imageObjectProtocolDirectoryImage()->String{
        return DataSourceRadiology.shared.imageDirectory
    }
    func imageObjectProtocolPhpFileToUpload()->URL{
        let uploadUrl = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/uploadFile.php")
        return uploadUrl!
    }
    func imageObjectProtocolPhpFileToDelete()->URL{
        let deleteUrl = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/deleteFileWithUrl.php")
        return deleteUrl!
    }
    func imageObjectProtocolPhpFileToGetSubDirectoryName()->URL?{
        let subDirectoryUrl = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/subDirectoryNameInImages.php")
        return subDirectoryUrl
    }
    
    //MARK: ImageManagerProtocol
    func rootUrlForImageObject()->URL?{
        return URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())")
    }
    //MARK: DataSourceProtocol
    
    func mainDirectoryName()->String{
        return "Radiology"
    }
    
    func utilizeMainBundle()->Bool{
        return Settings.UTILIZE_MAIN_BUNDLE
    }
    func baseUrlToRemoveFromMainBundle()->String{
        return Settings.BASE_URL_TO_REMOVE_FROM_MAIN_BUNDLE
    }
    func selectedCategory()->String?{
        return _selectedCategory
    }
    
    func baseDataNames()->[String]{
        var names = dictBaseDataCommonSigns.keys.sorted { (key0, key1) -> Bool in
            return key0.compare(key1) == ComparisonResult.orderedAscending
        }
        let namesCategory = dictBaseDataForCategory.keys.sorted { (key0, key1) -> Bool in
            return key0.compare(key1) == ComparisonResult.orderedAscending
        }
        names.append(contentsOf: namesCategory)
        return names
    }
    /**
     This return base data of groups of group of signs
     Example of group are Onset, otherData, baseRadiologySigns, CommonSigns, Sign for Category
     param: name is the name of the group of sign es. baseRadiologySignsKey
     */
    func baseData(with name:String)->BaseData?{
        var result: BaseData?
        if name == DataSourceRadiology.diseaseOnsetKey{
            result = self.onsetBaseData
        }
        if name == DataSourceRadiology.baseRadiologySignsKey{
            result = self.baseRadiologySignsBaseData
        }
        if name == DataSourceRadiology.otherDataKey{
            result = self.otherDataBaseData
        }
        if result == nil{
            result = self.dictBaseDataCommonSigns[name]
        }
        if result == nil{
            result = self.dictBaseDataForCategory[name]
        }
        return result
    }
    func addBaseDataToOtherSign(with arrayDataSource:[DataSourceObject], groupName:String, completion: @escaping (Bool, Error?) -> Void){
            let baseData = BaseData(name: groupName, element: nil)
            baseData.addObjects(dataSourceObjects: arrayDataSource)
            //print("Base data element: \n\(baseData.element.toString())")
            
            if let category = self.selectedCategory(){
                //if let existingBaseData = self.dictBaseDataForCategory[groupName]{
                //    baseData.addObjects(from: existingBaseData)
                //}
                self.dictBaseDataForCategory[groupName] = baseData
                
                let xmlElement = MyXMLElement(name: "root")
                for (_, data) in self.dictBaseDataForCategory{
                    xmlElement.add(child: data.element)
                }
                
                DataSourceRadiology.saveBaseDataXMLElement(for: category, element: xmlElement) { (success, identifier, error) -> Void in
                    completion(success, error)
                }
            }else{
                completion(false, nil)
                print("ERROR: No category selected")
            }
    }
    //Data for diseases
    var dictBaseDataForCategory = [String:BaseData]()//data in a dict where the key is the name of category and the BaseData contain the Base data for that category
    var dictBaseDataCommonSigns = [String:BaseData]()
    var onsetBaseData: BaseData?
    var baseRadiologySignsBaseData: BaseData?
    var otherDataBaseData: BaseData?

    //Data for category and groups
    var categoriesBaseData: BaseData?//Base data containing data of category categories (As DatasourceObject)
    var dictBaseDataGroupsForCategory =  [String:BaseData]() //dict containing base data of groups (As DatasourceObject)
    
    //General data for every group
    var dictGeneralDataGroups = [String:[DataSourceObjectCollection]]()//dict containing general data of groups like Anatomy, physiology ecc organized in a collection of DatasourceObjects
    var arrayGeneralDataNames = ["Anatomy","Anatomic_Variant", "Embriology", "Physiology", "Pathology", "Tecnique", "Pitfall", "Traumatology", "Post-surgery", "Report"]
   
    
    /**
     This return base data of groups of diseases (As DatasourceObject)
     Example of group are "neuro_anatomy", "intracranial_bleeding", "ischemic", "brain_tumor" for Neuro section
     param: category: is the name of the group es. neuro_anatomy
     */
    func baseDataGroup(for category:String)->BaseData?{
        return dictBaseDataGroupsForCategory[category]
    }
    //This are for a specific category

    static let diseaseOnsetKey = "diseaseOnset"
    static let baseRadiologySignsKey = "baseRadiologySigns"
    static let otherDataKey = "otherData"

    static let diseaseCategoriesKey = "categories"

    //static let clinicalSignsKey = "clinicalSigns"
    static let signsKey = "signs"

    
    func executeBlockOnAllBaseData(block:@escaping (_ baseDataName:String, _ dataSourceObjectName:String, _ dataSourceObject:DataSourceObject)->Void){
        for baseDataName in self.baseDataNames(){
            if let baseData = DataSourceRadiology.shared.baseData(with: baseDataName){
                for (key, object) in baseData.dictData{
                    block(baseDataName, key, object)
                }
            }
        }
    }
    func executeBlockOnBaseData(with name:String, block:@escaping (_ dataSourceObjectName:String, _ dataSourceObject:DataSourceObject)->Void){
        if let baseData = DataSourceRadiology.shared.baseData(with: name){
            for (key, object) in baseData.dictData{
                block(key, object)
            }
        }
    }
    //execute a block for an array of objectName for a basedata with a given name
    func executeBlockOnBaseData(with name:String, forObjectNames:[String], block:@escaping (_ dataSourceObjectName:String, _ dataSourceObject:DataSourceObject)->Void){
        if let baseData = DataSourceRadiology.shared.baseData(with: name){
            for (key, object) in baseData.dictData{
                if forObjectNames.contains(key){
                    block(key, object)
                }
            }
        }
    }
    //execute a block for a dict of basedataName:objectName
    func executeBlock(for dict:[String:[String]], block:@escaping (_ dataSourceObjectName:String, _ dataSourceObject:DataSourceObject)->Void){
        for (baseDataName, objectName) in dict{
            self.executeBlockOnBaseData(with: baseDataName, forObjectNames: objectName, block: block)
        }
    }
    
    
    //MARK: Load Data
    
    //Load category
    func loadCategories(utilizeFileManager: Bool, withCompletion:@escaping (_ error:Error?, _ element:MyXMLElement?)->Void){
        let baseUrl = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/BaseData")!
        let url = baseUrl.appendingPathComponent("Categories.xml")

        DataSource.shared.downloadXML(from: url, utilizeFileManager: utilizeFileManager) { [weak self](error, element) in
            if let categoriesElement = element?.getChild(with: DataSourceRadiology.diseaseCategoriesKey){
                self?.categoriesBaseData = BaseData(element: categoriesElement)
            }
            withCompletion(error, element)
        }
    }

    //This load base data equal to all the category
    func loadBaseDataCommon(utilizeFileManager: Bool, withCompletion:@escaping (_ error:Error?, _ element:MyXMLElement?)->Void){
        let baseUrl = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/BaseData")!
        let url = baseUrl.appendingPathComponent("baseData.xml")
        //print("downloading \(url)")
        DataSource.shared.downloadXML(from: url, utilizeFileManager: utilizeFileManager) { [weak self](error, element) in
            if let onsetElement = element?.getChild(with: DataSourceRadiology.diseaseOnsetKey){
                self?.onsetBaseData = BaseData(element: onsetElement)
            }
            if let baseRadiologySignsElement = element?.getChild(with: DataSourceRadiology.baseRadiologySignsKey){
                self?.baseRadiologySignsBaseData = BaseData(element: baseRadiologySignsElement)
                //generate imageObjects from image name
                if let dictData = self?.baseRadiologySignsBaseData?.dictData{
                    for (_, obj) in dictData{
                        if let imageName = obj.imageName{
                            let img = RadiologySignsDrawHelper.radiologySign(withName: imageName, size: 100)
                            obj.imageObject = ImageObject(image: img)
                            //obj.imageName = nil
                        }
                    }
                }                
            }
            if let otherDataElement = element?.getChild(with: DataSourceRadiology.otherDataKey){
                self?.otherDataBaseData = BaseData(element: otherDataElement)
            }
            let url = baseUrl.appendingPathComponent("baseDataCommonSigns.xml")
            //print(url)
            DataSource.shared.downloadXML(from: url, utilizeFileManager: utilizeFileManager) {[weak self] (error, element) in
                if let childs = element?.childs as? [MyXMLElement]{
                    for child in childs {
                        let baseData = BaseData(element: child)
                        self?.dictBaseDataCommonSigns[baseData.name] = baseData
                        //print("\(baseData.name): \(baseData)")
                    }
                }
                withCompletion(error, element)
                
            }
        }
    }
    
    func loadBaseDataGroupsForCategory(utilizeFileManager: Bool, withCompletion:@escaping (_ error:Error?, _ element:MyXMLElement?)->Void){
        let baseUrl = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/BaseData")!
        //Load radiologic signs
        let url = baseUrl.appendingPathComponent("baseDataGroupsForCategory.xml")
        //print(url)
        DataSource.shared.downloadXML(from: url, utilizeFileManager: utilizeFileManager) {[weak self] (error, element) in
            if let childs = element?.childs as? [MyXMLElement]{
                for child in childs {
                    let baseData = BaseData(element: child)
                    self?.dictBaseDataGroupsForCategory[baseData.name] = baseData
                }
            }
            withCompletion(error, element)
            
        }
    }
    
    func saveCategoryXMLElement(element:MyXMLElement, completion: @escaping (_ success:Bool, _ identifier:String?, _ error:Error?)->Void){
        let rootElement = MyXMLElement(name: "root")
        rootElement.add(child: element)
        DataSourceRadiology.saveXMLElementFile(element: rootElement, withFileName: "Categories.xml", inDirectory: "BaseData/", completion: completion)
    }
    
    func saveBaseDataGroupForCategory(withCompletion:@escaping (_ success:Bool,_ result:String?, _ error:Error?)->Void){
        let element = BaseData.xmlElement(for: self.dictBaseDataGroupsForCategory)
        DataSourceRadiology.saveXMLElementFile(element: element, withFileName: "baseDataGroupsForCategory.xml", inDirectory: "BaseData/", completion: withCompletion)
    }
    
    //This load specific data for a category
    func loadBaseDataOtherSigns(for category:String, utilizeFileManager: Bool, withCompletion:@escaping (_ error:Error?, _ element:MyXMLElement?)->Void){
        let baseUrl = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/DataSource/\(category)/BaseData")!
        //Load radiologic signs
        let url = baseUrl.appendingPathComponent("baseDataOtherSigns.xml")
        //print(url)
        DataSource.shared.downloadXML(from: url, utilizeFileManager: utilizeFileManager) {[weak self] (error, element) in
            
            
            if let childs = element?.childs as? [MyXMLElement]{
                for child in childs {
                    
                    let baseData = BaseData(element: child)
                    self?.dictBaseDataForCategory[baseData.name] = baseData
                }
            }
            withCompletion(error, element)
            
        }
    }
    //MARK: General data
    func addGeneralDataCollection(with dataSourceObjectCollection:DataSourceObjectCollection, groupName:String, completion: @escaping (Bool, Error?) -> Void){
        if let category = DataSource.shared.delegate.selectedCategory(){
            
            //Add the array to the dict
            
            var arrayGeneralDataGroup = self.dictGeneralDataGroups[groupName]
            if arrayGeneralDataGroup == nil{
                arrayGeneralDataGroup = [dataSourceObjectCollection]
                self.dictGeneralDataGroups[groupName] = arrayGeneralDataGroup
            }else{
                var indexToReplate:Int?
                for (index, existingCollection) in arrayGeneralDataGroup!.enumerated(){
                    if existingCollection.name == dataSourceObjectCollection.name{
                        indexToReplate = index
                        break
                    }
                }
                if let index = indexToReplate{
                    arrayGeneralDataGroup?[index] = dataSourceObjectCollection
                }else{
                    arrayGeneralDataGroup?.append(dataSourceObjectCollection)
                }
                self.dictGeneralDataGroups[groupName] = arrayGeneralDataGroup!

            }
            //generate the xml
            let xmlElement = MyXMLElement(name: "root")
            for (name, array) in self.dictGeneralDataGroups{
                let groupXmlElement = MyXMLElement(name: name)
                for collection in array{
                    let collectionElement = collection.generateXmlElement()
                    groupXmlElement.add(child: collectionElement)
                }
                xmlElement.add(child: groupXmlElement)
            }
            DataSourceRadiology.saveGeneralDataGroupXMLElement(for: category, element: xmlElement, completion: { (success, identifier, error) in
                completion(success, error)
            })
        }else{
            completion(false, nil)
            print("ERROR: No category selected")
        }
    }
    

    func loadGeneralDataGroups(for category:String, utilizeFileManager: Bool, withCompletion:@escaping (_ error:Error?, _ element:MyXMLElement?)->Void){
        let baseUrl = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/DataSource/\(category)/BaseData")!
        //Load radiologic signs
        let url = baseUrl.appendingPathComponent("generalDataGroups.xml")
        //print(url)
        //Remove all to avoid that object with same value will be showed
        self.dictGeneralDataGroups.removeAll()
        DataSource.shared.downloadXML(from: url, utilizeFileManager: utilizeFileManager) {[weak self] (error, element) in
            
            if let childs = element?.childs as? [MyXMLElement]{
                for childGroup in childs {
                    if let childsGroup = childGroup.childs as? [MyXMLElement]{
                        var arrayObjectGroups = [DataSourceObjectCollection]()
                        for childObjectCollection in childsGroup{
                            let objectGroup = DataSourceObjectCollection(xmlElement: childObjectCollection)
                            arrayObjectGroups.append(objectGroup)
                        }
                        self?.dictGeneralDataGroups[childGroup.name] = arrayObjectGroups
                    }
                }
            }
            withCompletion(error, element)
            
        }
    }
    //MARK: Delete GeneralDataCollection
    func deleteGeneralDataCollection(with dataSourceObjectCollection:DataSourceObjectCollection,
                                     inCategory:String,
                                     groupName:String,
                                     withCompletionImageObjectRemoved: ((_ string:String?)->Void)?,
                                     completion: @escaping (Bool, Error?) -> Void){
        
        
        //remove Images
        if let dataSourceObj = dataSourceObjectCollection.objects.last{
            dataSourceObjectCollection.objects.removeLast()
            if let imageObject = dataSourceObj.imageObject{
                ImageManager.shared.deleteImageObject(imageObject: imageObject, withCompletionBlock: {[weak self] (data, response, error) in
                    var result:String?
                    if let d = data{
                        result = PostObject.stringifyData(d)
                    }
                    withCompletionImageObjectRemoved?(result)
                    self?.deleteGeneralDataCollection(with: dataSourceObjectCollection,
                                                      inCategory:inCategory,
                                                      groupName: groupName,
                                                      withCompletionImageObjectRemoved: withCompletionImageObjectRemoved,
                                                      completion: completion)
                })
                
            }
            return
        }else{
            
            var arrayGeneralDataGroup = self.dictGeneralDataGroups[groupName]
            if arrayGeneralDataGroup != nil{
                var indexToRemove:Int?
                for (index, existingCollection) in arrayGeneralDataGroup!.enumerated(){
                    if existingCollection.name == dataSourceObjectCollection.name{
                        indexToRemove = index
                        break
                    }
                }
                if let index = indexToRemove{
                    arrayGeneralDataGroup?.remove(at: index)
                }
                self.dictGeneralDataGroups[groupName] = arrayGeneralDataGroup
                if arrayGeneralDataGroup?.count == 0{
                    self.dictGeneralDataGroups.removeValue(forKey: groupName)
                }
            }
            //generate the xml
            let xmlElement = MyXMLElement(name: "root")
            for (name, array) in self.dictGeneralDataGroups{
                let groupXmlElement = MyXMLElement(name: name)
                for collection in array{
                    let collectionElement = collection.generateXmlElement()
                    groupXmlElement.add(child: collectionElement)
                }
                xmlElement.add(child: groupXmlElement)
            }
            DataSourceRadiology.saveGeneralDataGroupXMLElement(for: inCategory, element: xmlElement, completion: { (success, identifier, error) in
                completion(success, error)
            })
        }
    }


    //MARK: Delete Disease
    func deleteRemotely(disease:Disease,
                        in category:String,
                        withCompletionImageObjectRemoved: ((_ string:String?)->Void)?,
                        withCompletionDirectoryImagesRemoved: ((_ string:String?)->Void)?,
                        withCompletionDiseaseRemoved: ((_ string:String?)->Void)?){
        //remove from table and xml
        if let identifier = disease.diseaseIdentifier{
            
            //remove images
            if let imgObject = disease.diseaseImages.last{
                disease.diseaseImages.removeLast()
                ImageManager.shared.deleteImageObject(imageObject: imgObject, withCompletionBlock: {[weak self] (data, response, error) in
                    var result:String?
                    if let d = data{
                        result = PostObject.stringifyData(d)
                    }
                    withCompletionImageObjectRemoved?(result)
                    self?.deleteRemotely(disease: disease, in: category,
                                         withCompletionImageObjectRemoved: withCompletionDiseaseRemoved,
                                         withCompletionDirectoryImagesRemoved: withCompletionDirectoryImagesRemoved,
                                         withCompletionDiseaseRemoved: withCompletionDiseaseRemoved)
                    
                })
                return
            }else{
                //remove image directory
                ImageManager.fileNameImage(for: identifier, completion: { (success, error, fileName) in
                    if let fName = fileName{
                        let url = "DataSource/\(category)/Images/\(fName)"
                        let postDeleteDirectory = PostObject(with: self.deletePhpFile!)
                        postDeleteDirectory.sendAsynchronousPost(withDict: ["url":url], withCompletionBlock: { (data, response, error) in
                            var result:String?
                            if let d = data{
                                result = PostObject.stringifyData(d)
                            }
                            withCompletionDirectoryImagesRemoved?(result)
                            //remove disease
                            let postDeleteXml = PostObject(with: self.deletePhpDisease!)
                            postDeleteXml.sendAsynchronousPost(withDict: ["identifier":identifier], withCompletionBlock: { (data, response, error) in
                                var result:String?
                                if let d = data{
                                    result = PostObject.stringifyData(d)
                                }
                                withCompletionDiseaseRemoved?(result)

                            })
                        })
                    }
                })
            }
        }
    }

    
    //MARK: get disease list
    static func listOfDiseases(from listOfXMLElements:[String:MyXMLElement])->[Disease]{
        var diseases = [Disease]()
        for identifier in listOfXMLElements.keys{
            if let e = listOfXMLElements[identifier]{
                diseases.append(Disease(xmlElement: e, identifier: identifier))
            }
        }
        //sort name
        diseases.sort { (disease1, disease2) -> Bool in
            return disease1.diseaseName.localizedCaseInsensitiveCompare(disease2.diseaseName) == ComparisonResult.orderedAscending
        }
        return diseases
    }
    

    
    //MARK: Load no xml data
    static let male = "male"
    static let female = "female"
    static let age = "age"
    static let sex = "sex"

    //Sex and age
    func ageImage(for age:Int)->NSImage?{
        switch age {
        case 0...10:
            return NSImage(named: "folderImage")
        default:
            return NSImage(named: "folderImage")
        }
    }
    
    func sexObject(forMale isMale:Bool)->DataSourceObject?{
        if isMale{
            let obj = DataSourceObject(value: DataSourceRadiology.male.localized, dictForLang: nil, imageName: "male", imageObject: nil, frequency: nil)
            obj.addNameAndDescriptionForCurrentLanguage(name: DataSourceRadiology.male.localized, description: nil)
            return obj
        }else{
            let obj = DataSourceObject(value: DataSourceRadiology.female.localized, dictForLang: nil, imageName: "female", imageObject: nil, frequency: nil)
            obj.addNameAndDescriptionForCurrentLanguage(name: DataSourceRadiology.female.localized, description: nil)
            return obj
        }
    }

    //Onset
    static let acuteOnset = "acuteOnset"
    static let subacuteOnset = "subacuteOnset"
    static let chronicOnset = "chronicOnset"
    static let acuteSubacuteOnset = "acuteSubacuteOnset"
    static let subacuteChronicOnset = "subacuteChronicOnset"
    static let variableOnset = "variableOnset"

    
    static func arrayContainsRadiologicSignCombo(for baseDataName:String, array:[String]) -> [[String]]?{
        var result = [[String]]()
        if let comboArray = DataSourceRadiology.shared.baseData(with: baseDataName)?.comboArray{
            for values in comboArray{
                if array.containsAllValues(values: values){
                    result.append(values)
                }
            }
        }
        if result.count > 0{
            return result
        }
        return nil
    }
    //MARK: DataSource downloading data
    static func listOfGroups(for category:String, with completion:@escaping (_ success:Bool, _ error:Error?, _ groups:[String]?)->Void){
        if let url = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/listOfGroupForCategory.php"){
            let post = PostObject(with: url)
            
            post.sendAsynchronousPost(withDict: ["category":category], withCompletionBlock: { (data, response, error) in
                var success = false
                var arrayResult:[String]?
                if error == nil{
                    do{
                        let array = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        arrayResult = array as? [String]
                        success = true
                    } catch{
                        print("error in Deserialization data source in Group")
                        //completion(success: false, error: error, list: nil)
                    }
                }
                completion(success, error, arrayResult)
                
                
            })
            
        }
    }
    
    
    static private func loadDataSourceList(forCategory category:String, group:String, with completion:@escaping (_ error:Error?, _ list:[String:String]?)->Void){
        if let url = URL(string: "http://46.22.208.75/apps/\(DataSource.shared.delegate.mainDirectoryName())/dataSourceList.php"){
            let post = PostObject(with: url)
            
            
            post.sendAsynchronousPost(withDict: ["group":group, "category":category], withCompletionBlock: { (data, response, error) in
                var dict = [String:String]()
                if error == nil{
                    do{
                        
                        let responseDict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        
                        if let responseDictUnw = responseDict as? [String:String]{
                            dict = responseDictUnw
                        }
                    } catch{
                        print("error in Deserialization data source in DataSourceList")
                        //completion(success: false, error: error, list: nil)
                    }
                }
                completion(error, dict)
                
            })
            
        }
    }
    
    
    static func loadDataSource(forCategory category:String,
                               group:String,
                               utilizeFileManager: Bool,
                               completionFile:@escaping (_ error:Error?, _ fileName:String?)->Void,
                               completionFinal:@escaping (_ error:Error?, _ dataSourceDict:[String:MyXMLElement])->Void){
        var dict = [String:MyXMLElement]()
        DataSourceRadiology.loadDataSourceList(forCategory: category, group: group, with: { (errorList, list) in
            if let fileNameList = list{
                for (key, fileUrl) in fileNameList{
                    DataSource.shared.downloadXML(from: DataSource.shared.completeUrl(for:fileUrl), utilizeFileManager: utilizeFileManager,  completion: { (error, element) -> Void in
                        if let e = element{
                            //let diseaseFileName = name.stringByDeletingPathExtension
                            dict[key] = e
                            completionFile(error, fileUrl)
                        }
                        if dict.count == fileNameList.count{//last object
                            completionFinal(errorList, dict)
                        }
                    })
                }
                if fileNameList.count == 0{
                    completionFinal(errorList, dict)
                }
            }else{
                completionFinal(errorList, dict)
            }
            
        })
    }
    
    //MARK: Saving XML
    static func saveBaseDataXMLElement(for category:String, element:MyXMLElement, completion: @escaping (_ success:Bool, _ identifier:String?, _ error:Error?)->Void){
        let postUpload = PostObject(with: URL(string: "http://46.22.208.75/apps/\(DataSourceRadiology.shared.mainDirectoryName())/uploadBaseDataXML.php")!)
        let dict = ["category":category]
        let doc = MyXMLDocument(root: element)
        if let dataUnw = doc.toData(){
            
            postUpload.uploadAsynchronous(data: dataUnw, withFileName: "baseDataOtherSigns.xml", dictPost: dict) { (data, response, error) in
                if let d = data{
                    //print( PostObject.stringifyData(d))
                    completion(true, PostObject.stringifyData(d), error)
                }else{
                    completion(false, nil, error)
                }
            }
        }else{
            print("Error in saving XML Base data because of data encoding error")
        }
        
    }
    static func saveGeneralDataGroupXMLElement(for category:String, element:MyXMLElement, completion: @escaping (_ success:Bool, _ identifier:String?, _ error:Error?)->Void){
        let postUpload = PostObject(with: URL(string: "http://46.22.208.75/apps/\(DataSourceRadiology.shared.mainDirectoryName())/uploadBaseDataXML.php")!)
        let dict = ["category":category]
        let doc = MyXMLDocument(root: element)
        if let dataUnw = doc.toData(){
            
            postUpload.uploadAsynchronous(data: dataUnw, withFileName: "generalDataGroups.xml", dictPost: dict) { (data, response, error) in
                if let d = data{
                    //print( PostObject.stringifyData(d))
                    completion(true, PostObject.stringifyData(d), error)
                }else{
                    completion(false, nil, error)
                }
            }
        }else{
            print("Error in saving XML Base data because of data encoding error")
        }
        
    }
    
    static func saveXMLElementFile(element:MyXMLElement, withFileName:String, inDirectory:String, completion: @escaping (_ success:Bool, _ identifier:String?, _ error:Error?)->Void){
        let postUpload = PostObject(with: URL(string: "http://46.22.208.75/apps/\(DataSourceRadiology.shared.mainDirectoryName())/uploadXMLFileInDirectory.php")!)
        let doc = MyXMLDocument(root: element)
        if let dataUnw = doc.toData(){
            
            postUpload.uploadAsynchronous(data: dataUnw, withFileName: withFileName, dictPost: ["directory":inDirectory]) { (data, response, error) in
                if let d = data{
                    //print( PostObject.stringifyData(d))
                    completion(true, PostObject.stringifyData(d), error)
                }else{
                    completion(false, nil, error)
                }
            }
        }else{
            print("Error in saving XML Category because of data encoding error")
        }
        
    }
    
    
    static func saveXMLElement(with identifier:String?, group:String, category:String, diseaseName:String, element:MyXMLElement, completion: @escaping (_ success:Bool, _ identifier:String?, _ error:Error?)->Void){
        let postUpload = PostObject(with: URL(string: "http://46.22.208.75/apps/\(DataSourceRadiology.shared.mainDirectoryName())/uploadXML.php")!)
        var dict = ["preferredFileName":diseaseName]
        dict["group"] = group
        dict["category"] = category
        if let identifierUnw = identifier{
            dict["identifier"] = identifierUnw
            
        }
        let doc = MyXMLDocument(root: element)
        if let dataUnw = doc.toData(){
            
            postUpload.uploadAsynchronous(data: dataUnw, withFileName: diseaseName, dictPost: dict) { (data, response, error) in
                if let d = data{
                    completion(true, PostObject.stringifyData(d), error)
                }else{
                    completion(false, identifier, error)
                }
            }
        }else{
            print("Error in saving XML disease because of data encoding error")
        }
        
    }
}
