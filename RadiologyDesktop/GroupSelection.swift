//
//  GroupSelection.swift
//  RadiologyDesktop
//
//  Created by Alessandro Ferrazza on 21/08/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import MacBaseKit
import SharedFilesForMac


class GroupSelection: DataSourceList, DataSourceListProtocol {

    @IBAction func reloadData(sender:NSToolbarItem){
        self.reloadDataSource()
    }
    
    var category:String?
    
    
    var isLoading = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.reloadDataSource()
        
        //1. SET THE DIRECTORY TO SAVE
        if let category = self.category{
            self.savingDirectoryForImage = "BaseData/CategoryAndGroupImages/\(category)/"
        }

    }
    
    
    //MARK:Saving
    //2. SAVE THE FILE
    override func saveFileRemotely(with completion: @escaping (Error?) -> Void) {
        if let category = self.category{
            
            if self.dataSourceRemovedValues.count > 0{
                if let baseData = DataSourceRadiology.shared.baseDataGroup(for: category){
                    for value in self.dataSourceRemovedValues{
                        baseData.dictData.removeValue(forKey: value)
                        
                    }
                    baseData.updateXMLElement()
                    self.dataSourceRemovedValues.removeAll()

                }
            }
            if let newObject = self.dataSourceAdded.last{
                self.loadingLabel(loading: true, with: "Saving group with value: \(newObject.value)", color: nil)
                self.dataSourceAdded.removeLast()
                //save data
                
                if let baseData = DataSourceRadiology.shared.baseDataGroup(for: category){
                    baseData.setObject(dataSourceObjects: newObject, for: newObject.value)
                }else{
                    let baseData = BaseData(name: category, objects: [newObject])
                    DataSourceRadiology.shared.dictBaseDataGroupsForCategory[category] = baseData
                }
                self.saveFileRemotely(with: completion)
                
            }else{
                DataSourceRadiology.shared.saveBaseDataGroupForCategory(withCompletion: { (success, result, error) in
                    completion(error)
                })
            }
        }else{
            completion(nil)
        }
    }
    
    func reloadDataSource(){
        if let cat = self.category{
            self.progressionIndicator?.isHidden = false
            if isLoading == false{
                self.isLoading = true
                self.loadingLabel(loading: false, with: "Downloading signs for category.", color: nil)
                
                DataSourceRadiology.shared.loadBaseDataOtherSigns(for: cat, utilizeFileManager: false) { [weak self] (error, element) in
                    if let category = self?.category{
                        self?.loadingLabel(loading: false, with: "Downloading general data for groups.", color: nil)
                        DataSourceRadiology.shared.loadGeneralDataGroups(for: category, utilizeFileManager: false, withCompletion: { (error, element) in
                            DispatchQueue.main.async {
                                
                                self?.showUpdatedMessage(for: (element != nil) , error: error)
                                self?.loadDataFromBaseData()
                                self?.isLoading = false
                            }
                        })
                    }
                }
            }
        }else{
            self.progressionIndicator?.isHidden = true

        }
        
    }
    
    func loadDataFromBaseData(){
        if let category = self.category{
            if let baseDataDict = DataSourceRadiology.shared.baseDataGroup(for: category)?.dictData{
                self.dataSource = Array(baseDataDict.values)
            }else{
                self.dataSource = [DataSourceObject]()
            }
            self.tableView.reloadData()
        }
        
    }
   
    //MARK: Segue
    func dataSourceListProtocolSelected(object: DataSourceObject, with dataSourceList: DataSourceList) {
        
        self.performSegue(withIdentifier: "GroupSegue", sender: self)
        
    }
    
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if segue.identifier == "GroupSegue"{
            if let indexPath = self.indexPathForSelectedRow(){
                let object = self.object(at: indexPath)
                let group = object.value
                if let nextScene = segue.destinationController as? ListOfDiseases{
                    nextScene.category = category
                    nextScene.group = group
                    if let generalDataGroup = DataSourceRadiology.shared.dictGeneralDataGroups[group]{
                        nextScene.generalDataSource = generalDataGroup
                    }
                    if let split = self.splitViewController as? MySplitController{
                        nextScene.splitViewController = split
                        split.addViewControllerToStack(viewController: nextScene)
                    }

                }
            }
        }
    }
    
    override func tableView(_ tableView: NSTableView, titleForHeaderInSection section: Int) -> String? {
        
        //Return somethings othrwise the action cell woll work not properly
        return "Groups"
    }
    
}
