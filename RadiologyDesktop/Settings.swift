//
//  Settings.swift
//  RadiologyDesktop
//
//  Created by Alessandro Ferrazza on 08/09/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa

class Settings {
    static var translationColumnHidden = true

    static let isDeveloperApp = true
    static let UTILIZE_MAIN_BUNDLE = false
    static let UTILIZE_FILE_MANAGER = true

    static let BASE_URL_TO_REMOVE_FROM_MAIN_BUNDLE = "/apps/Radiology/"
    
    static func color(for category:String?) -> NSColor{
        let result = NSColor.controlBackgroundColor
        if let c = category{
            switch c {
            case "liver":
                return NSColor(calibratedRed: 1, green: 0.7, blue: 0.6, alpha: 0.3)
            case "Pancreas":
                return NSColor.red.withAlphaComponent(0.3)
            case "Kidney":
                return NSColor.yellow.withAlphaComponent(0.3)
            default:
                return NSColor.controlBackgroundColor
            }
        }
        return result
        
    }
    
    static func language(for tableColumn:NSTableColumn?, in tableView:NSTableView) -> String{
        if let column = tableColumn{
            if column == tableView.tableColumns[0]{
                return "ita"
            }else{
                return "eng"
            }
        }
        return "ita"

    }
}
