//
//  ViewerSuperClass.swift
//  RadiologyDesktop
//
//  Created by Alessandro Ferrazza on 25/08/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import MacBaseKit
import SharedFilesForMac

//The subclassess must immplement this protocol
protocol ViewerSuperClassProtocol {
    func save(titleText:String, at indexPath:IndexPath, for language:String)
    func save(detailText:String, at indexPath:IndexPath, for language:String)
}
class ViewerSuperClass: LoadingViewController, NSTextFieldDelegate, SubtitleImageButtonTableCellViewProtocol {

    var selectedIndexPath:IndexPath? = nil
    var selectedTextFieldTag:Int? = nil
    var group:String?
    var category:String?
    
    let translationLanguage = "eng"
    var unsavedChange = false
    
    static let itaTfTagTitle = 1
    static let itaTfTagDetail = 2
    static let translationTfTagTitle = 3
    static let translationTfTagDetail = 4

    static let saveNotificationNameKey = "saveNotificationName"

    //these method and property are to perform some setup in table when needed.
    //In fact view did load is called after tableview heightforCell, so height for cell must do some setup in table but only at first call
    private var basicTableSetupPerformed = false
    func basicTableSetup(){
        //perform once
        if self.basicTableSetupPerformed == false{
            self.basicTableSetupPerformed = true
            self.tableView.columnAutoresizingStyle = NSTableView.ColumnAutoresizingStyle.uniformColumnAutoresizingStyle
            self.hideTranslationColumn(hide: Settings.translationColumnHidden)
        }
    }
    
    override func tableView(_ tableView: NSTableView, cellForRowAt indexPath: IndexPath, for tableColumn: NSTableColumn?) -> NSView? {
        var cellIdentifier = "subtitleTableCellView"
        if self.imageObject(at: indexPath) == nil || tableColumn == tableView.tableColumns[1]{
            //no image if there is no image and if the colum is the transalate one
            cellIdentifier = "subtitleTableCellView_noImage"
        }
        let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: cellIdentifier), owner: nil)
        if let imageButtonCell = cell as? SubtitleImageButtonTableCellView{
            imageButtonCell.delegate = self
        }
        return cell
    }
    
    //MARK: SubtitleImageButtonTableCellViewProtocol
    func subtitleImageButtonTableCellView(cell: SubtitleImageButtonTableCellView) {
        if let indexPath = self.indexPath(for: cell){
            let storyboard = NSStoryboard(name: "Main", bundle: nil)
            if let imageObject = imageObject(at: indexPath){
                let vc = storyboard.instantiateController(withIdentifier: "ImageObjectVC") as! ImageObjectVC
                vc.utilizeFileManager = Settings.UTILIZE_FILE_MANAGER
                vc.isViewer = true
                vc.imageObject = imageObject
                if let description = self.description(at: indexPath){
                    vc.initialText = description
                    
                }
                self.presentAsSheet(vc)
            }
        }
        
    }
    func description(at indexPath:IndexPath)->String?{
        return nil
    }
    func imageObject(at indexPath:IndexPath)->ImageObject?{
        return nil
    }
    override func tableView(_ tableView: NSTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        self.basicTableSetup()
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
   
    @IBAction func actionHideTranslationColumn(sender:NSButton){
        if Settings.translationColumnHidden{
            Settings.translationColumnHidden = false
        }else{
            Settings.translationColumnHidden = true
        }
        self.hideTranslationColumn(hide: Settings.translationColumnHidden)
    }
    func hideTranslationColumn(hide:Bool){
        Settings.translationColumnHidden = hide
        let columnTranslationIndex = self.tableView.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue:"TranslationColumnId"))
        let column = self.tableView.tableColumns[columnTranslationIndex]
        self.tableView.beginUpdates()
        column.isHidden = Settings.translationColumnHidden
        self.tableView.endUpdates()
    }
    
    
    func sendSaveNotification(){
        NotificationCenter.default.post(name: NSNotification.Name(ViewerSuperClass.saveNotificationNameKey), object: nil, userInfo: nil)
    }
    
    //MARK: command bar and pop ups
    @IBAction func save(sender:NSToolbarItem){
        self.saveAction()
    }
    
    @IBAction func translate(sender:NSToolbarItem){
        self.translateAction()
    }
 
    @IBAction func addSignAction(sender:NSToolbarItem){
        self.addAction()
    }
    
    func saveAction(){
        self.view.window?.makeFirstResponder(nil)
        //to implement in subclasses
    }
    func translateAction(){
        self.view.window?.makeFirstResponder(nil)
        //to implement in subclasses
    }
    func addAction(){
        //to implement in subclasses
    }
    
    func removeAction(at indexPath:IndexPath){
        //to implement in subclasses
    }
    
    override func tableView(_ tableView: NSTableView, actionsForRowAtIndexPath indexPath: IndexPath, forEdge edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        var actions = [NSTableViewRowAction]()
        if edge == NSTableView.RowActionEdge.trailing{
            let action = NSTableViewRowAction(style: NSTableViewRowAction.Style.destructive, title: "Delete") { [weak self](action, row) in
                self?.removeAction(at: indexPath)
            }
            action.image = NSImage(named: NSImage.trashEmptyName)
            actions.append(action)
        }
        return actions
    }
    
    override func tableView(_ tableView: NSTableView, colorForHeaderInSection section: Int) -> NSColor? {
        return Settings.color(for: self.category)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //set the j tag dark blue
        if #available(OSX 10.13, *) {
            NSAttributedString.addAttribute(forTag: "j", key: NSAttributedString.Key.foregroundColor, obj: NSColor(named: "blueControlColor") as Any)
            //here utilize the control color named blueControlColor present in assets in order to support dark and light mode
        } else {
            NSAttributedString.addAttribute(forTag: "j", key: NSAttributedString.Key.foregroundColor, obj: NSColor.myDarkBlue)
        }
        
    }
    
    
    //MARK: NSTextField delegate method
    //this is to avoid that the tab and the enter will end editing the textfields
    func control(_ control: NSControl, textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
        var result = false
        if commandSelector == #selector(NSResponder.insertNewline(_:)){
            textView.insertNewlineIgnoringFieldEditor(self)
            result = true
            
        }else if commandSelector == #selector(NSResponder.insertTab(_:)){
            textView.insertTabIgnoringFieldEditor(self)
            result = true
            
        }
        return result
    }
    func controlTextDidEndEditing(_ obj: Notification) {
        if let tf = obj.object as? NSTextField{
            var text = tf.stringValue
            if tf.tag == 2 || tf.tag == 4{//detail
                text = tf.attributedStringValue.convertToTaggedString()
            }
            if let ip = self.selectedIndexPath{
                self.save(text: text, at: ip, for: tf.tag)
            }
        }
    }
    
    //MARK: Saving To implement
    func save(text:String, at indexPath:IndexPath, for textFieldTag:Int){
        var language = "ita"
        if textFieldTag == 3 || textFieldTag == 4{//Translation language
            language = self.translationLanguage
        }
        if textFieldTag == 1 || textFieldTag == 3{//Title
            self.save(titleText: text, at: indexPath, for: language)
        }else if textFieldTag == 2 || textFieldTag == 4{//detail
            self.save(detailText: text, at: indexPath, for: language)
        }
    }
    open func save(titleText:String, at indexPath:IndexPath, for language:String){
        
    }
    open func save(detailText:String, at indexPath:IndexPath, for language:String){
        
    }
    
    //MARK: Alerts
    /*
    func setProgression(with percent:Float){
        self.progressionIndicator.isIndeterminate = false
        self.progressionIndicator.isHidden = false
        self.progressionIndicator.doubleValue = Double(percent)
    }
    */
    func showAlertSaveUnchanged()->Bool?{
        return self.showAlert(with: "Ci sono modifiche non salvate nell'attuale malattia, vuoi salvarle?")
    }
    
    
    
    func showAlert(with message:String)->Bool?{
        let alert = NSAlert()
        alert.messageText = message
        alert.addButton(withTitle: "Si")// 1000
        alert.addButton(withTitle: "No")// 1001
        alert.addButton(withTitle: "Cancel")// 1002
        alert.showsSuppressionButton = false
        alert.alertStyle = NSAlert.Style.critical
        let responseTag = alert.runModal()
        if responseTag.rawValue == 1000{
            return true
        }else if responseTag.rawValue == 1001{
            return false
        }else{
            return nil
        }
    }
    
        
    
    
    func showTranslateMessage(for success:Bool, error:Error?){
        
        var text = ""
        var color:NSColor?
        if error == nil{
            text = "Testo tradotto con successo!"
            color = NSColor(calibratedRed: 0, green: 0.5, blue: 0, alpha: 1)
        }else if let e = error{
            text = "Errore durante la traduzione: \(e.localizedDescription)"
            color = NSColor(calibratedRed: 0.5, green: 0, blue: 0, alpha: 1)
        }
        self.loadingLabel(loading: false, with: text, color: color)
    }
    
}
