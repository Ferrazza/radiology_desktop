//
//  WindowController.swift
//  RadiologyDesktop
//
//  Created by Alessandro Ferrazza on 08/09/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import MacBaseKit
import SharedFilesForMac

class WindowController: NSWindowController {

    
    func splitViewController()->SplitController?{
        return self.window?.contentViewController as? SplitController
    }

    //action of toolbar for left view controller
    @IBAction func backAction(sender:NSToolbarItem){
        if let split = self.splitViewController() as? MySplitController{
            split.backToPreviousViewController()
        }
    }
    
    @IBAction func removeAllFilesSaved(sender:NSToolbarItem){
        if let leftVC = self.splitViewController()?.children[0] as? LoadingViewController{
            FileObject.shared.removeAllFileInDocDirectory()
            leftVC.loadingLabel(loading: false, with: "Removed All Files in file manager", color: NSColor.myDarkGreen)
            
        }
        
        
    }
    
    @IBAction func backToRootAction(sender:NSToolbarItem){
        if let split = self.splitViewController() as? MySplitController{
            split.backToRootViewController()
        }
        
    }
    
    override func windowDidLoad() {
        super.windowDidLoad()
        //UserDefaults.standard.set(false, forKey: "NSConstraintBasedLayoutVisualizeMutuallyExclusiveConstraints")

        if let window = self.window{
            //window.titleVisibility = NSWindowTitleVisibility.hidden
            //window.isOpaque = false
            window.titlebarAppearsTransparent = true
            
            window.toolbar?.sizeMode = NSToolbar.SizeMode.small
            //window.contentView?.wantsLayer = true
            //window.appearance = NSAppearance(named: NSAppearanceNameVibrantLight)
            //window.invalidateShadow()
            window.toolbar?.showsBaselineSeparator = true
            window.backgroundColor = NSColor(calibratedRed: 0.9, green: 0.9, blue: 0.9, alpha: 1)
            
            if let versionDict = Bundle.main.infoDictionary{
                if let version = versionDict["CFBundleShortVersionString"] as? String{
                    window.title = "Radiology Desktop v. "  + version
                }
            }
            /*
            if let content = window.contentView{
                let blurryView = NSVisualEffectView(frame:content.bounds)
                blurryView.wantsLayer = true
                blurryView.blendingMode = NSVisualEffectBlendingMode.behindWindow
                blurryView.material = NSVisualEffectMaterial.light
                blurryView.state = NSVisualEffectState.active
                
                //content.addSubview(blurryView)

            }
            */
        }
                // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    }

}
