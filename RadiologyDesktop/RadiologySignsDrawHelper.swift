//
//  RadiologySignsDrawHelper.swift
//  RadiologyDesktop
//
//  Created by Alessandro Ferrazza on 11/09/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import MacBaseKit

enum ColorSigns:CGFloat {
    case an = 0
    case ipo = 0.3
    case iso = 0.55
    case iper = 0.7
    case ipoMarked = 0.1
    case iperMarked = 0.9
    case restriction = 0.05
    case notRestriction = 1
    
    case reduceSignal = 0.2
    case increaseSignal = 0.95
    case sameSignal = 0.5

    
    static func colorSign(for name:String) -> ColorSigns {
        switch name {
        case "an":
            return ColorSigns.an
        case "ipo":
            return ColorSigns.ipo
        case "iso":
            return ColorSigns.iso
        case "iper":
            return ColorSigns.iper
        case "ipoMarked":
            return ColorSigns.ipoMarked
        case "iperMarked":
            return ColorSigns.iperMarked
        case "restriction":
            return ColorSigns.restriction
        case "notRestriction":
            return ColorSigns.notRestriction
        case "reduceSignal":
            return ColorSigns.reduceSignal
        case "increaseSignal":
            return ColorSigns.increaseSignal
        case "sameSignal":
            return ColorSigns.sameSignal
        default:
            return ColorSigns.iso
        }
    }
}
enum ColorPhase {
    case arterial
    case portal
    case late
    case rientro
    case basal

    static func colorPhase(for name:String) -> ColorPhase {
        switch name {
        case "arterial":
            return ColorPhase.arterial
        case "portal":
            return ColorPhase.portal
        case "late":
            return ColorPhase.late
        case "rientro":
            return ColorPhase.rientro

        default://basal
            return ColorPhase.basal
        }
    }
    
    static func color(for colorPhase:ColorPhase) -> NSColor{
        switch colorPhase {
        case ColorPhase.arterial:
            return NSColor.red
        case ColorPhase.portal:
            return NSColor.blue
        case ColorPhase.late:
            return NSColor.green
        case ColorPhase.rientro:
            return NSColor.yellow
        default://basal
            return NSColor(calibratedWhite: 0.5, alpha: 1)
        }
    }
    
}

class RadiologySignsDrawHelper: DrawHelper {

    
    static func radiologySign(withName name: String, size: CGFloat) -> NSImage {
       
        
        
        //get components
        let array = name.components(separatedBy: "_")
        
        //get color
        var signComponent = ColorSigns.colorSign(for: array[0])
        var phaseComponent = ColorPhase.basal
        if array.count > 1{
            phaseComponent = ColorPhase.colorPhase(for: array[0])
            signComponent = ColorSigns.colorSign(for: array[1])
        }
        
        let signColor = NSColor(calibratedWhite: signComponent.rawValue, alpha: 1)
        var phaseColor = ColorPhase.color(for: phaseComponent)
        
        
        //get backgroundColor
        var backgroundColor = NSColor(calibratedWhite: 0.5, alpha: 1)
        if signComponent == ColorSigns.restriction || signComponent == ColorSigns.notRestriction{
            backgroundColor = NSColor(calibratedWhite: 0.9, alpha: 1)
        }
        
        if signComponent == ColorSigns.increaseSignal || signComponent == ColorSigns.reduceSignal || signComponent == ColorSigns.sameSignal{
            backgroundColor = NSColor(calibratedWhite: 0.9, alpha: 1)
            //create to images for in and opp
            return self.image(with: signColor, backgroundColor: backgroundColor, size: size) { (ctx) in
                let realSize = size/2
                //imageIn
                //Draw phase
                var inset = realSize / 10
                var radius: CGFloat = realSize / 2 - inset
                var offset = CGSize(width: size/4 - radius, height: size/2 - radius)
                phaseColor = backgroundColor
                let baseColor = NSColor(calibratedWhite: ColorSigns.sameSignal.rawValue, alpha: 1)
                
                RadiologySignsDrawHelper.drawCircolarPath(ctx: ctx, color: baseColor, size: realSize, inset: inset, offset: offset)
                
                //Draw sign
                inset = realSize / 5
                radius = realSize / 2 - inset
                offset = CGSize(width: size/4 - radius, height: size/2 - radius)
                RadiologySignsDrawHelper.drawCircolarPath(ctx: ctx, color: baseColor, size: realSize, inset: inset, offset: offset)
                
                //imageOut
                //Draw phase
                inset = realSize / 10
                radius = realSize / 2 - inset
                offset = CGSize(width: size/4*3 - radius, height: size/2 - radius)
                phaseColor = NSColor.black
                RadiologySignsDrawHelper.drawCircolarPath(ctx: ctx, color: phaseColor, size: realSize, inset: inset, offset: offset)
                
                //Draw sign
                inset = realSize / 5
                radius = realSize / 2 - inset
                offset = CGSize(width: size/4*3 - radius, height: size/2 - radius)
                RadiologySignsDrawHelper.drawCircolarPath(ctx: ctx, color: signColor, size: realSize, inset: inset, offset: offset)
                
            }
            
        }else{
            return self.image(with: signColor, backgroundColor: backgroundColor, size: size) { (ctx) in
                
                //Draw phase
                var inset = size / 10
                RadiologySignsDrawHelper.drawCircolarPath(ctx: ctx, color: phaseColor, size: size, inset: inset)
                
                //Draw sign
                inset = size / 5
                RadiologySignsDrawHelper.drawCircolarPath(ctx: ctx, color: signColor, size: size, inset: inset)
                
            }
        }
        
    }
    
    static func drawCircolarPath(ctx:CGContext?, color:NSColor, size:CGFloat, inset:CGFloat){
        let radius: CGFloat = size / 2 - inset
        RadiologySignsDrawHelper.drawCircolarPath(ctx: ctx,
                                                  color: color,
                                                  size: size,
                                                  inset: inset,
                                                  offset: CGSize(width: size / 2 - radius, height: size / 2 - radius))
    }
    
    static func drawCircolarPath(ctx:CGContext?, color:NSColor, size:CGFloat, inset:CGFloat, offset:CGSize){
        //get radius
        let radius: CGFloat = size / 2 - inset
        let path: CGMutablePath = DrawHelper.circolarPathEmpty(with: 0,
                                                               finalAngle: .pi * 2,
                                                               radius: radius,
                                                               offset: offset)
        ctx?.addPath(path)
        ctx?.setFillColor(color.cgColor)
        ctx?.setStrokeColor(color.cgColor)
        ctx?.fillPath()
        ctx?.strokePath()
    }
    
}
