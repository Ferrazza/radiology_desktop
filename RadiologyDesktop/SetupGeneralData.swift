//
//  SetupGeneralData.swift
//  RadiologyDesktop
//
//  Created by Alessandro Ferrazza on 10/09/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import MacBaseKit
import SharedFilesForMac

protocol SetupGeneralDataProtocol {
    func setupGeneralDataProtocolDidEnd(with setupGeneralData:SetupGeneralData?)
    func setupGeneralDataProtocolDidCancel(with setupGeneralData:SetupGeneralData?)
}


class SetupGeneralData: NSViewController, DataSourceListProtocol, NSTextFieldDelegate {

    @IBOutlet var imageButton: NSButton!
    @IBOutlet var onsetLabel:NSTextField!
    @IBOutlet var ageRangeLabel:NSTextField!
    @IBOutlet var ageRangeMinTextField:NSTextField!
    @IBOutlet var ageRangeMaxTextField:NSTextField!
    @IBOutlet var ratioMFMaleTextField:NSTextField!
    @IBOutlet var ratioMFFemaleTextField:NSTextField!
    @IBOutlet var progressIndicator: NSProgressIndicator!

    var delegate: SetupGeneralDataProtocol?
    var onset:DataSourceObject?
    var ageRanges = [AgeRange]()
    var maleFemaleRatio:MaleFemaleRatio?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ageRangeMinTextField.delegate = self
        self.ageRangeMaxTextField.delegate = self
        self.ratioMFMaleTextField.delegate = self
        self.ratioMFFemaleTextField.delegate = self
        self.progressIndicator.isHidden = true
        self.loadData(withMaleToFemaleRatio: true)
    }
    
    func setupMaleToFemaleRatioFromTextFields(){
        self.ratioMFMaleTextField.resignFirstResponder()
        self.ratioMFFemaleTextField.resignFirstResponder()
        if self.ratioMFMaleTextField.stringValue.count > 0 && self.ratioMFMaleTextField.stringValue.count > 0{
            self.maleFemaleRatio = MaleFemaleRatio(male: self.ratioMFMaleTextField.stringValue.intValue, female:  self.ratioMFFemaleTextField.stringValue.intValue)
        }
    }
    
    func loadData(withMaleToFemaleRatio:Bool){
        //Onset
        if let name = self.onset?.name{
            self.onsetLabel.stringValue = name
        }
        if let imgName = self.onset?.imageName{
            self.imageButton.image = NSImage(named: imgName)
        }else if let imgObj = self.onset?.imageObject{
            if let img = imgObj.image{
                self.imageButton.image = img
            }else if let imgUrl = imgObj.imageUrl{
                self.progressIndicator.isIndeterminate = false
                self.progressIndicator.isHidden = false
                self.progressIndicator.startAnimation(nil)
                
                let downloader = ImageDownloader(with: imgUrl)
                downloader.downloadImage(percentDownloadBlock: {[weak self](percentDownload) in
                    self?.progressIndicator.doubleValue = Double(percentDownload)
                    }, completionBlock: { [weak self](image, error, downloaded) in
                        if let img = image{
                            DispatchQueue.main.async {
                                self?.progressIndicator.stopAnimation(nil)
                                self?.progressIndicator.isHidden = true
                                self?.imageButton.image = img
                            }
                        }
                })
            }
        }
        //Age ranges
        var maleToFamaleRatioText = ""
        for (index, range) in self.ageRanges.enumerated(){
            maleToFamaleRatioText = maleToFamaleRatioText + "\(range.min)-\(range.max) yrs"
            if index != self.ageRanges.count - 1{//not the last
                maleToFamaleRatioText = maleToFamaleRatioText + "\n"
            }
        }
        self.ageRangeLabel.stringValue = maleToFamaleRatioText
        self.ageRangeMaxTextField.stringValue = ""
        self.ageRangeMinTextField.stringValue = ""

        //Male to Female ratio
        if withMaleToFemaleRatio{
            if let ratio = self.maleFemaleRatio{
                self.ratioMFMaleTextField.stringValue = "\(ratio.male)"
                self.ratioMFFemaleTextField.stringValue = "\(ratio.female)"
            }
        }
        
        
    }
    
    func controlTextDidEndEditing(_ obj: Notification) {
        if let tf = obj.object as? NSTextField{
            if tf == self.ratioMFFemaleTextField || tf == self.ratioMFMaleTextField{
                self.setupMaleToFemaleRatioFromTextFields()
            }
        }
    }
    @IBAction func addAgeRange(sender:NSButton){
        if self.ageRangeMinTextField.stringValue.count > 0 && self.ageRangeMaxTextField.stringValue.count > 0{
            let ageRange = AgeRange(min: self.ageRangeMinTextField.stringValue.intValue, max: self.ageRangeMaxTextField.stringValue.intValue)
            self.ageRanges.append(ageRange)
            self.loadData(withMaleToFemaleRatio: false)
            
        }
    }

    @IBAction func removeAllAgeRanges(sender:NSButton){
        self.ageRanges.removeAll()
        self.loadData(withMaleToFemaleRatio: false)
    }

    @IBAction func okButtonAction(sender:NSButton){
        self.setupMaleToFemaleRatioFromTextFields()
        self.delegate?.setupGeneralDataProtocolDidEnd(with: self)
    }
    
    @IBAction func cancelButtonAction(sender:NSButton){
        self.delegate?.setupGeneralDataProtocolDidCancel(with: self)
        
        
    }
    
    @IBAction func imageAction(sender:NSButton){
        
        var array = [DataSourceObject]()
        if let onsetData = DataSourceRadiology.shared.baseData(with: DataSourceRadiology.diseaseOnsetKey)?.dictData{
            for (_ ,value) in onsetData{
                array.append(value)
            }
        }
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateController(withIdentifier: "ObjectSelection") as! ObjectSelection
        vc.dataSource = array
        vc.delegate = self
        let animator = TransitionAnimator()
        animator.transition = [NSViewController.TransitionOptions.slideLeft, NSViewController.TransitionOptions.crossfade]
        self.present(vc, animator: animator)
        //}else{
        //    print("cannot change image because saved remotely. Create a new image object")
        //}
    }
    
    func dataSourceListProtocolSelected(object: DataSourceObject, with dataSourceList: DataSourceList) {
        self.onset = object
        self.loadData(withMaleToFemaleRatio: false)
        self.dismiss(dataSourceList)
    }
}
