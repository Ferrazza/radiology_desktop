//
//  CorrespondanceCalculator.swift
//  Radiology
//
//  Created by Alessandro Ferrazza on 23/10/16.
//  Copyright © 2016 Alessandro Ferrazza. All rights reserved.
//

import MacBaseKit
import SharedFilesForMac

class CorrespondanceCalculator: NSObject {
    
    

    var onset:String?
    var signsDict = [String:[String]]() //name of base data is the name and value is the array of sign name
    var isMale:Bool?
    var age:Int?

    static let colorDict:[CorrespondanceCalculator.Correspondence:NSColor] = [CorrespondanceCalculator.Correspondence.correspond: NSColor.myDarkBlue,
                            CorrespondanceCalculator.Correspondence.nearCorrespond: NSColor.myDarkYellow,
                            CorrespondanceCalculator.Correspondence.notCorrespond: NSColor.myDarkRed]
    
    /*
    var radiologicSignObjects:[DataSourceObject]{
        get{
            var result = [DataSourceObject]()
            for sign in self.radiologicSigns{
                if let signObj = DataSourceRadiology.shared.radiologicSignsData[sign]{
                    result.append(signObj)
                }
            }
            return result
        }
    }
    var clinicalSignObjects:[DataSourceObject]{
        get{
            var result = [DataSourceObject]()
            for sign in self.clinicalSigns{
                if let signObj = DataSourceRadiology.shared.clinicalSignsData[sign]{
                    result.append(signObj)
                }
            }
            return result
        }
    }
    */
    func removeAll(){
        self.onset = nil
        self.signsDict = [String:[String]]()
        //self.radiologicSigns = [String]()
        //self.clinicalSigns = [String]()
        self.isMale = nil
        self.age = nil
    }
    enum Correspondence:Int, Equatable {
        case correspond = 2, nearCorrespond = 1, notCorrespond = 0
        
    }
    static func correspondenceIsGreater(_ correspondence:Correspondence, thanCorrespondence:Correspondence)->Bool{
        return correspondence.rawValue < thanCorrespondence.rawValue
    }
    
    static func compareOnset(_ onset:String, diseaseOnset:String)->Correspondence{
        switch onset {
        case DataSourceRadiology.acuteOnset:
            switch diseaseOnset {
            case DataSourceRadiology.acuteOnset:
                return Correspondence.correspond
            case DataSourceRadiology.acuteSubacuteOnset:
                return Correspondence.correspond
            case DataSourceRadiology.variableOnset:
                return Correspondence.correspond
            case DataSourceRadiology.subacuteOnset:
                return Correspondence.nearCorrespond
            case DataSourceRadiology.subacuteChronicOnset:
                return Correspondence.notCorrespond
            case DataSourceRadiology.chronicOnset:
                return Correspondence.notCorrespond
            default:
                return Correspondence.notCorrespond
            }
        case DataSourceRadiology.subacuteOnset:
            switch diseaseOnset {
            case DataSourceRadiology.acuteOnset:
                return Correspondence.nearCorrespond
            case DataSourceRadiology.acuteSubacuteOnset:
                return Correspondence.correspond
            case DataSourceRadiology.variableOnset:
                return Correspondence.correspond
            case DataSourceRadiology.subacuteOnset:
                return Correspondence.correspond
            case DataSourceRadiology.subacuteChronicOnset:
                return Correspondence.nearCorrespond
            case DataSourceRadiology.chronicOnset:
                return Correspondence.notCorrespond
            default:
                return Correspondence.notCorrespond
            }
        default://Chronic
            switch diseaseOnset {
            case DataSourceRadiology.acuteOnset:
                return Correspondence.notCorrespond
            case DataSourceRadiology.acuteSubacuteOnset:
                return Correspondence.notCorrespond
            case DataSourceRadiology.variableOnset:
                return Correspondence.correspond
            case DataSourceRadiology.subacuteOnset:
                return Correspondence.notCorrespond
            case DataSourceRadiology.subacuteChronicOnset:
                return Correspondence.correspond
            case DataSourceRadiology.chronicOnset:
                return Correspondence.correspond
            default:
                return Correspondence.notCorrespond
            }
        }
    }
    
    static func compareAge(_ age:Int, diseaseRanges:[AgeRange])->Correspondence{
        
        var correspond = Correspondence.notCorrespond
        for range in diseaseRanges{
            if range.contains(age){
                correspond = Correspondence.correspond
                break
            }
        }
        return correspond
    }
    static func compareSex(_ isMale:Bool, maleFemaleRatio:MaleFemaleRatio)->(correspondence:Correspondence, value:Float){
        if maleFemaleRatio.male == maleFemaleRatio.female{
            return (Correspondence.correspond, 0)
        }else{
            var negativeValue = 1
            if isMale{
                if maleFemaleRatio.male > maleFemaleRatio.female{
                    return (Correspondence.correspond, 1)
                }else{
                    negativeValue = maleFemaleRatio.female
                }
            }else{
                if maleFemaleRatio.female > maleFemaleRatio.male{
                    return (Correspondence.correspond, 1)
                }else{
                    negativeValue = maleFemaleRatio.male
                }
            }
            var value:Float = 1
            switch negativeValue {
            case 1...10:
                value = 1
            case 11...100:
                value = 2
            case 101...1000:
                value = 3
            case 1001...10000:
                value = 4
            default:
                value = 5
            }
            return (Correspondence.notCorrespond, value)
        }
    }
    
    
    //-----------------------------------
    //----------III PROVA---------
    //Sorted on base of points of disease
    //----------------------------
    func getScore(for disease:Disease)->(result:Float, dictSignsNamesNotFound:[String:[String]], toRemove:Bool){
        return CorrespondanceCalculator.getScore(for:disease, onsetCorrespondence: self.onset, signsCorrespondence: self.signsDict, sexCorrespondance: self.isMale, ageCorrespondance: self.age)
    }
    fileprivate static func getScore(for disease:Disease, onsetCorrespondence:String?, signsCorrespondence:[String:[String]]?, sexCorrespondance:Bool?, ageCorrespondance:Int?)->(result:Float, dictSignsNamesNotFound:[String:[String]], toRemove:Bool){
        
        var result:Float = 0
        var resultDictSignsNamesNotFound = [String:[String]]()
        var toRemove = false

        if let age = ageCorrespondance{
            if disease.diseaseAgeRanges.count > 0{
                let correspond = CorrespondanceCalculator.compareAge(age, diseaseRanges: disease.diseaseAgeRanges)
                if correspond == Correspondence.correspond{
                    result = result + 1
                }else{
                    result = result - 1
                }
            }
        }
        if let isMale = sexCorrespondance{
            if let mFRatio = disease.diseaseMaleFemaleRatio{
                let correspond = CorrespondanceCalculator.compareSex(isMale, maleFemaleRatio: mFRatio)
                if correspond.correspondence == Correspondence.correspond{
                    result = result + 1
                }else{
                    result = result - correspond.value
                }
            }
        }
        if let o = onsetCorrespondence{
            var diseaseCorrespondence = Correspondence.notCorrespond
            if let diseaseOnset = disease.diseaseOnset{
                diseaseCorrespondence = CorrespondanceCalculator.compareOnset(o, diseaseOnset: diseaseOnset)
            }
            if diseaseCorrespondence.rawValue > 0{
                result = result + Float(diseaseCorrespondence.rawValue)/2
            }else{
                result = result - 1
            }
        }
        if let dict = signsCorrespondence{
            let tuple = CorrespondanceCalculator.check(for: dict, diseaseSignsObjs: disease.diseaseSigns)
            result = result + tuple.result
            resultDictSignsNamesNotFound = tuple.dictSignsNamesNotFound
            if tuple.mustBeExcluded == true{
                toRemove = tuple.mustBeExcluded
            }
        }
        
        
        
        return (result, resultDictSignsNamesNotFound, toRemove)
    }
    
    fileprivate static func check(for dictSignsNames:[String:[String]], diseaseSignsObjs:[String:[Element]])->(result:Float, dictSignsNamesNotFound:[String:[String]], mustBeExcluded:Bool){
        //resultValuesNotFound is an array of value not found
        //mustBeExcluded is yes if the disease must be excluded
        var result:Float = 0
        var resultDictSignsNamesNotFound = [String:[String]]()
        var mustBeExcluded = false

        for (groupName, signNames) in dictSignsNames{
            if let elements = diseaseSignsObjs[groupName]{
                if mustBeExcluded == false{
                    mustBeExcluded = Disease.mustBeExcludedForSignsNamesAndArraySignObject(signNames, arraySignObj: elements)
                }
                for signName in signNames {
                    let contain = Disease.containsSignWithNameInArray(signName, arraySignObj: elements)
                    if contain.contain{
                        if let value = contain.value{
                            result = result + value
                        }else{
                            result = result + 1
                        }
                        
                    }else{
                        result = result - 1
                        if var notFoundNames = resultDictSignsNamesNotFound[groupName]{
                            notFoundNames.append(signName)
                        }else{
                            let notFoundNames = [signName]
                            resultDictSignsNamesNotFound[groupName] = notFoundNames
                        }
                    }
                }

            }
        }
        return (result, resultDictSignsNamesNotFound, mustBeExcluded)
    }
    
    func reorderList(of diseases:[Disease])->([Disease], [Disease]){
        return CorrespondanceCalculator.reorderList(of: diseases, onsetCorrespondence: self.onset, signsCorrespondence: self.signsDict, sexCorrespondance: self.isMale, ageCorrespondance: self.age)
    }
    fileprivate static func reorderList(of diseases:[Disease], onsetCorrespondence:String?, signsCorrespondence:[String:[String]]?, sexCorrespondance:Bool?, ageCorrespondance:Int?)->([Disease], [Disease]){
        var result = diseases
        
        //sort name
        result = result.insertionSort(result, { (disease1, disease2) -> Bool in
            return disease1.diseaseName.localizedCaseInsensitiveCompare(disease2.diseaseName) == ComparisonResult.orderedAscending
        })
        
        var diseasesRemoved = [Disease]()
        result = result.insertionSort(result, { (disease1, disease2) -> Bool in
            let score1 = CorrespondanceCalculator.getScore(for: disease1, onsetCorrespondence: onsetCorrespondence, signsCorrespondence:signsCorrespondence, sexCorrespondance: sexCorrespondance, ageCorrespondance: ageCorrespondance)
            let score2 = CorrespondanceCalculator.getScore(for: disease2, onsetCorrespondence: onsetCorrespondence, signsCorrespondence:signsCorrespondence, sexCorrespondance: sexCorrespondance, ageCorrespondance: ageCorrespondance)
            
            //remove diseases to remove
            if score1.toRemove == true{
                if diseasesRemoved.contains(disease1) == false{
                    diseasesRemoved.append(disease1)
                }
            }
            if score2.toRemove == true{
                if diseasesRemoved.contains(disease2) == false{
                    diseasesRemoved.append(disease2)
                }
            }
            return score2.result < score1.result
        })
        
        if Settings.isDeveloperApp == false{
            for diseaseToRemove in diseasesRemoved{
                if let index = result.index(of: diseaseToRemove){
                    result.remove(at: index)
                }
            }
            diseasesRemoved.removeAll()
        }
        
        return (result, diseasesRemoved)
        
    }
    
    //MARK: attributed string and calculators helpers
    func calculateCorrespondenceAndUnfoundSignsForDisease(_ disease:Disease?)->(correspondence:Float?, dictSignsNamesNotFound:[String:[String]]){
        var scoreDisease:Float = 0
        var dictSignsNamesNotFound = [String:[String]]()
        if let d = disease{
            let score = self.getScore(for: d)
            scoreDisease = score.result
            dictSignsNamesNotFound = score.dictSignsNamesNotFound
        }
        let correspondence = self.calculateCorrespondence(with: scoreDisease)
        return (correspondence, dictSignsNamesNotFound)
    }
    func calculateCorrespondence(with score:Float)->Float?{
        var total:Float = 0
        var calculate = false
        
        if self.onset != nil{
            calculate = true
            total = total + 1
        }
        DataSourceRadiology.shared.executeBlockOnAllBaseData { (baseDataName, objectName, object) in
            var toAdd:Float = 1
            if let maxValue = object.maxValue{
                toAdd = maxValue
            }
            total = total + toAdd
            calculate = true
        }

        /*
        for s in self.radiologicSigns{
            let sign = DataSourceRadiology.shared.radiologicSignsData[s]
            var toAdd:Float = 1
            if let maxValue = sign?.maxValue{
                toAdd = maxValue
            }
            total = total + toAdd
            calculate = true
        }
        for s in self.clinicalSigns{
            let sign = DataSourceRadiology.shared.clinicalSignsData[s]
            var toAdd:Float = 1
            if let maxValue = sign?.maxValue{
                toAdd = maxValue
            }
            total = total + toAdd
            calculate = true
        }
        */
        if self.age != nil{
            calculate = true
            total = total + 1
        }
        if self.isMale != nil{
            calculate = true
            total = total + 1
        }
        //lo score va da + total a - total
        let actualScore = score + total
        
        if calculate == true{
            return actualScore/(total * 2) * 100
        }else{
            return nil
        }
    }
    
    func color(for correspondance:Float)->NSColor{
        switch correspondance {
        case 0...30:
            return NSColor.red
            
        case 50...100:
            return NSColor.blue
        default:
            return NSColor.darkGray
        }
        
    }

    //MARK: AttributedString
    
    func attributedStringCorrispondence(for disease:Disease, fontSize:CGFloat, addOtherSigns:Bool, addClinicalSigns:Bool)->NSAttributedString{
        
        let result = NSMutableAttributedString()
        let plainFont = NSFont.systemFont(ofSize: fontSize)
        let boldFont = NSFont.boldSystemFont(ofSize: fontSize)
        let baseColor = NSColor.gray
        
        var addedFirstRow = false
        if let mFRatio = disease.diseaseMaleFemaleRatio{
            addedFirstRow = true
            let title = NSAttributedString(string: "M:F ratio".localized + ": ",
                                                attributes:[NSAttributedString.Key.font : boldFont])
            result.append(title)
            var color = baseColor
            if let isMaleCorr = self.isMale{
                let value = CorrespondanceCalculator.compareSex(isMaleCorr, maleFemaleRatio: mFRatio)
                if let c = CorrespondanceCalculator.colorDict[value.correspondence]{
                    color = c
                }
            }
            let string = "\(mFRatio.male):\(mFRatio.female)"
            let attrString = NSAttributedString(string: string,
                                                   attributes: [
                                                    NSAttributedString.Key.font : plainFont,
                                                    NSAttributedString.Key.foregroundColor : color])
            result.append(attrString)

        }
        
        if disease.diseaseAgeRanges.count > 0{
            if addedFirstRow == true{
                result.append(NSAttributedString(string:"\n"))
            }
            addedFirstRow = true
            //---------------------
            let title = NSAttributedString(string: "Age Ranges".localized + ": ",
                                           attributes:[NSAttributedString.Key.font : boldFont])
            result.append(title)
            var color = baseColor
            if let age = self.age{
                let corr = CorrespondanceCalculator.compareAge(age, diseaseRanges: disease.diseaseAgeRanges)
                if let c = CorrespondanceCalculator.colorDict[corr]{
                    color = c
                }
            }
            var string = ""
            for (index, range) in disease.diseaseAgeRanges.enumerated(){
                string = string + "\(range.min) - \(range.max)" + " " + "yrs".localized
                
                if index != disease.diseaseAgeRanges.count - 1 {
                    string = string + "; "
                }
                
            }
            let attrString = NSAttributedString(string: string,
                                                attributes: [
                                                    NSAttributedString.Key.font : plainFont,
                                                    NSAttributedString.Key.foregroundColor : color])
            result.append(attrString)
            
        }
        
        if let diseaseOnset = disease.diseaseOnset{
            if addedFirstRow == true{
                result.append(NSAttributedString(string:"\n"))
            }
            addedFirstRow = true
            //---------------------
            let onsetTitle = NSAttributedString(string: "Onset".localized + ": ",
                                                attributes:[NSAttributedString.Key.font : boldFont])
            result.append(onsetTitle)
            
            var color = baseColor
            if let on = self.onset{
                let correspondence = CorrespondanceCalculator.compareOnset(on, diseaseOnset: diseaseOnset)
                if let c = CorrespondanceCalculator.colorDict[correspondence]{
                    color = c
                }
            }
            
            
            var onsetString = diseaseOnset
            if let onsetObj = DataSourceRadiology.shared.onsetBaseData?.dictData[diseaseOnset]{
                onsetString = onsetObj.name
            }
            let onsetText = NSAttributedString(string: onsetString,
                                               attributes: [
                                                NSAttributedString.Key.font : plainFont,
                                                NSAttributedString.Key.foregroundColor : color])
            result.append(onsetText)
        }
        
        if addOtherSigns{
            for (key, elements) in disease.diseaseSigns{
                if let signStrings = self.signsDict[key], let dataSourceDict = DataSourceRadiology.shared.baseData(with:key)?.dictData {
                    let tuple = CorrespondanceCalculator.attributedString(for: elements,
                                                                          signsTitle: signStrings,
                                                                          signsData: dataSourceDict,
                                                                          title: key.localized,
                                                                          addedFirstRow: addedFirstRow,
                                                                          boldFont: boldFont,
                                                                          plainFont: plainFont,
                                                                          baseColor: baseColor)
                    result.append(tuple.attrStringResult)
                    addedFirstRow = tuple.addedFirstRowResult
                }
                
            }
            
        }
        if addClinicalSigns{
            if let clinicalSignElements = disease.diseaseSigns[DataSourceRadiology.commonClinicalSignsKey], let clinicalSignStrings = self.signsDict[DataSourceRadiology.commonClinicalSignsKey], let dataSourceDict = DataSourceRadiology.shared.baseData(with: DataSourceRadiology.commonClinicalSignsKey)?.dictData {
                let tuple = CorrespondanceCalculator.attributedString(for: clinicalSignElements,
                                                                      signsTitle: clinicalSignStrings,
                                                                      signsData: dataSourceDict,
                                                                      title: "Clinical Signs".localized,
                                                                      addedFirstRow: addedFirstRow,
                                                                      boldFont: boldFont,
                                                                      plainFont: plainFont,
                                                                      baseColor: baseColor)
                result.append(tuple.attrStringResult)
                addedFirstRow = tuple.addedFirstRowResult

            }
        }
        
        return result
    }
    
    
    
    static func attributedString(for arrayOfSigns:[Element], signsTitle:[String], signsData:[String:DataSourceObject], title:String, addedFirstRow:Bool, boldFont:NSFont, plainFont:NSFont, baseColor:NSColor)->(attrStringResult:NSAttributedString, addedFirstRowResult:Bool) {
        let result = NSMutableAttributedString()
        var addedFirstRowResult = false
        let tuple = Element.arrayOfElementWithoutExcluded(arrayOfSigns)
        let signs = tuple.array
        let excludedSigns = tuple.excluded
        
        if signs.count > 0{
            if addedFirstRow == true{
                result.append(NSAttributedString(string:"\n"))
            }
            addedFirstRowResult = true
            //---------------------
            let signsTitle = NSAttributedString(string: title + ":\n",
                                                attributes:[NSAttributedString.Key.font : boldFont])
            result.append(signsTitle)
        }
        for (index, sign) in signs.enumerated(){
            //Name
            var signName = sign.name
            //Color
            var color = baseColor
            if signsTitle.contains(signName){
                color = CorrespondanceCalculator.colorDict[CorrespondanceCalculator.Correspondence.correspond]!
            }
            
            //Readable Name
            if let signObj = signsData[signName]{
                signName = signObj.name
            }
            //if developer the sign name are also the value and exclude property
            if Settings.isDeveloperApp{
                signName = sign.getNameAndDataString(from:signsData)
            }
            
            result.append(NSAttributedString(string: " - \(signName)",
                attributes: [
                    NSAttributedString.Key.font : plainFont,
                    NSAttributedString.Key.foregroundColor : color]))
            
            if index < signs.count - 1{
                result.append(NSAttributedString(string:"\n"))
            }
        }
        if Settings.isDeveloperApp{
            let color = CorrespondanceCalculator.colorDict[CorrespondanceCalculator.Correspondence.notCorrespond]!
            if excludedSigns.count > 0{
                result.append(NSAttributedString(string:"\n"))
                //---------------------
                let signsTitle = NSAttributedString(string: "Excluding Signs".localized + ":\n",
                                                    attributes:[NSAttributedString.Key.font : boldFont,
                                                                NSAttributedString.Key.foregroundColor : color])
                result.append(signsTitle)
            }
            for (index, sign) in excludedSigns.enumerated(){
                //Name
                var signName = sign.name
                if let signObj = signsData[signName]{
                    signName = signObj.name
                }
                signName = sign.getNameAndDataString(from:signsData)
                //Color
                result.append(NSAttributedString(string: " - \(signName)",
                    attributes: [NSAttributedString.Key.font : plainFont,
                                 NSAttributedString.Key.foregroundColor : color]))
                
                if index < signs.count - 1{
                    result.append(NSAttributedString(string:"\n"))
                }
            }
        }
        return (result, addedFirstRowResult)
    }
    
    static func attributedStringDescription(for disease:Disease, fontSize:CGFloat)->NSAttributedString?{
        let result = NSMutableAttributedString()
        
        let plainFont = NSFont.systemFont(ofSize: fontSize)
        //let baseColor = NSColor.grayColor()
        
        var returnResult = false
        if let d = disease.diseaseDescription{
            returnResult = true
            let description = NSAttributedString(string: d,
                                                 attributes:[NSAttributedString.Key.font : plainFont])
            result.append(description)
        }
        if returnResult{
            return result
        }else{
            return nil
        }
    }
    
    static func attributedString(for disease:Disease, fontSize:CGFloat, addOtherSigns:Bool, addClinicalSigns:Bool)->NSAttributedString{
        let result = NSMutableAttributedString()
        
        /*
        let boldFont = NSFont.boldSystemFont(ofSize: fontSize)
        let plainFont = NSFont.systemFont(ofSize: fontSize)
        let baseColor = NSColor.gray
        */
        
        let nullCorrespondence = CorrespondanceCalculator()
        result.append(nullCorrespondence.attributedStringCorrispondence(for: disease, fontSize: fontSize, addOtherSigns: addOtherSigns, addClinicalSigns: addClinicalSigns))
        return result
    }
    

    static func appendNotFoundSigns(_ dictSigns:[String:[String]]?, toAttributeString:NSAttributedString?, fontSize:CGFloat)->NSAttributedString?{
        if let dict = dictSigns, let unwToAttrString = toAttributeString{
            let plainFont = NSFont.systemFont(ofSize: fontSize)
            let boldFont = NSFont.boldSystemFont(ofSize: fontSize)
            
            let result = NSMutableAttributedString()
            result.append(unwToAttrString)
            result.append(NSAttributedString(string:"\n"))
            let signsTitle = NSAttributedString(string: "Not corresponding signs".localized + ": ",
                                                attributes:[
                                                    NSAttributedString.Key.font : boldFont,
                                                    NSAttributedString.Key.foregroundColor : CorrespondanceCalculator.colorDict[CorrespondanceCalculator.Correspondence.notCorrespond]!])
                                                    
                                                    
            result.append(signsTitle)
        
            for (groupName, signs) in dict{
                result.append(NSAttributedString(string:"\n"))
                let groupAttrString = NSAttributedString(string: groupName,
                                                       attributes: [
                                                        NSAttributedString.Key.font : plainFont,
                                                        NSAttributedString.Key.foregroundColor : CorrespondanceCalculator.colorDict[CorrespondanceCalculator.Correspondence.notCorrespond]!])
                result.append(groupAttrString)

                for sign in signs{
                    var signName = sign
                    if let signObj = DataSourceRadiology.shared.baseData(with: groupName)?.dictData[sign]{
                        signName = signObj.name
                    }
                    result.append(NSAttributedString(string:"\n"))
                    let disAttrString = NSAttributedString(string: " - " + signName,
                                                           attributes: [
                                                            NSAttributedString.Key.font : plainFont,
                                                            NSAttributedString.Key.foregroundColor : CorrespondanceCalculator.colorDict[CorrespondanceCalculator.Correspondence.notCorrespond]!])
                    result.append(disAttrString)
                    
                }
            }
            
            
            
            return result
        }else{
            return toAttributeString
        }
    }



}
