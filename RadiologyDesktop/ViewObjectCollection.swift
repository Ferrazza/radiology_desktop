//
//  ViewObjectCollection.swift
//  RadiologyDesktop
//
//  Created by Alessandro Ferrazza on 25/08/17.
//  Copyright © 2017 Alessandro Ferrazza. All rights reserved.
//

import Cocoa
import MacBaseKit
import SharedFilesForMac

class ViewObjectCollection: ViewerSuperClass, ViewerSuperClassProtocol, SetupDataSourceObjectVCProtocol {
    
    
    
    var objectCollection = DataSourceObjectCollection(name: "noName"){
        didSet{
            self.resetDataSourceDict(with: self.objectCollection.objects)
        }
    }

    var categorized = true
    var dataSourceDict = [String:[DataSourceObject]]()
    var arrayCategories = [String]()

    func setObjectCollection(objectCollection:DataSourceObjectCollection?){
        
        self.unsavedChange = false
        if let obj = objectCollection{
            self.objectCollection = obj
        }
        //self.diseaseTraslation = self.copyDiseaseForLanguage(disease: disease, language: self.translationLanguage)
        self.tableView.reloadData()
        self.loadingLabel(loading: false, with: "", color: nil)
        
    }
    //MARK: command bar and pop ups -> action implemented from superclass
    override func saveAction(){
        super.saveAction()
        self.save()
    }
    
    override func translateAction() {
        super.translateAction()
        self.translateRow()
    }
    
    override func addAction() {
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateController(withIdentifier: "SetupDataSourceObjectVC") as! SetupDataSourceObjectVC
        vc.delegate = self
        vc.showAccessoryData = false
        //set the category and progressive number if a row is selected
        
        vc.progressiveNumber = 0
        if let ip = self.selectedIndexPath{
            let selectedObj = self.object(at: ip)
            if let cat = selectedObj.category{
                if let array = self.dataSourceDict[cat]{
                    vc.progressiveNumber = array.count
                }
            }
            vc.setupTextFieldCategory(with: selectedObj.category)
            
        }
 
        //self.presentAsModalWindow(vc)
        self.presentAsSheet(vc)
        
        
    }
    
    override func removeAction(at indexPath: IndexPath) {
        
        self.unsavedChange = true
        let obj = object(at: indexPath)
        if let imgObject = obj.imageObject{
            self.imageObjectsToRemove.append(imgObject)
        }
        self.objectCollection.remove(object: obj)
        self.removeRows(at: [indexPath], withAnimation: NSTableView.AnimationOptions.effectFade)
    }
    //MARK: Managing obj
    static var nameAndDescriptionKey = "Name and Description"
    func resetDataSourceDict(with array:[DataSourceObject]){
        self.dataSourceDict = [String:[DataSourceObject]]()//dict of category

        self.dataSourceDict[ViewObjectCollection.nameAndDescriptionKey] = [self.objectCollection.nameAndDescriptionObj]
        let orderedData = self.objectCollection.objects.sorted { (obj1, obj2) -> Bool in
            return obj1.value.compare(obj2.value) == ComparisonResult.orderedAscending
        }
        
        var addToArrayCategory = false
        if self.arrayCategories.isEmpty{//if arrayCategory is set before (for example to give a given order), it will not be created here
            addToArrayCategory = true
        }
        if addToArrayCategory{
            self.arrayCategories = self.arrayCategories.sorted()
        }
        
        for obj in orderedData{
            var category = "Other".localized
            if let c = obj.category{
                category = c
            }
            if self.arrayCategories.contains(category) == false{
                self.arrayCategories.append(category)
            }
            
            if var array = self.dataSourceDict[category]{
                array.append(obj)
                self.dataSourceDict[category] = array
                
            }else{
                let array = [obj]
                self.dataSourceDict[category] = array
                if addToArrayCategory{
                    if self.arrayCategories.contains(category) == false{
                        self.arrayCategories.append(category)
                    }
                }
            }
        }
        //remove empty category in array category provided
        if addToArrayCategory == false{
            let keysInDataSourceDict = Array(self.dataSourceDict.keys)
            var arrayIndexesToRemove = [Int]()
            for (index, category) in self.arrayCategories.enumerated(){
                if keysInDataSourceDict.contains(category) == false{
                    arrayIndexesToRemove.append(index)
                }
            }
            _ = self.arrayCategories.removeAtIndexes(arrayIndexesToRemove)
        }
    }
    
    func object(at indexPath:IndexPath)->DataSourceObject{
        if self.categorized{
            let value = self.sectionTitleAndArray(at: indexPath.section)
            let array = value.array
            return array[indexPath.item]
        }else{
            return self.objectCollection.objects[indexPath.item]
        }
    }
    override func imageObject(at indexPath: IndexPath) -> ImageObject? {
        return self.object(at: indexPath).imageObject
    }
    override func description(at indexPath: IndexPath) -> String? {
        
        let obj = self.object(at: indexPath)
        let description = obj.description(for: SharedKeys.languageKey.localized)
        
        return description
    }
    
    func sectionTitleAndArray(at section:Int) -> (sectionTitle:String, array:[DataSourceObject]){
        if self.dataSourceDict.count > 0{
            let keys = Array(self.dataSourceDict.keys)
            var sortedKeys = keys.sorted()
            
            //removeNameAndDescriptions
            if let indexNameAndDescription = sortedKeys.index(of: ViewObjectCollection.nameAndDescriptionKey){
                sortedKeys.remove(at: indexNameAndDescription)
            }
            //add name and description at index 0
            sortedKeys.insert(ViewObjectCollection.nameAndDescriptionKey, at: 0)
            
            let sectionTitle = sortedKeys[section]
            let array = self.dataSourceDict[sectionTitle]
            return (sectionTitle, array!)
        }else{
            return ("", [DataSourceObject]())
        }
        
    }

    //MARK: Tableview delegate and dataSource

    override func numberOfSections(in tableView: NSTableView) -> Int {
        if self.categorized{
            return self.dataSourceDict.count
        }
        return 1
    }
    
    override func tableView(_ tableView: NSTableView, numberOfRowsInSection section: Int) -> Int {
        if self.categorized{
            let value = self.sectionTitleAndArray(at: section)
            return value.array.count
        }
        return self.objectCollection.objects.count
    }
    
    override func tableView(_ tableView: NSTableView, titleForHeaderInSection section: Int) -> String? {
        if self.categorized{
            return self.sectionTitleAndArray(at: section).sectionTitle
        }
        return nil
    }
    
    override func tableView(_ tableView: NSTableView, cellForRowAt indexPath: IndexPath, for tableColumn: NSTableColumn?) -> NSView? {
        let language = Settings.language(for: tableColumn, in: tableView)
        let obj = self.object(at: indexPath)
        if let cell = super.tableView(tableView, cellForRowAt: indexPath, for: tableColumn) as? SubtitleTableCellView {
            //CELL SETUP
            //Set the tag to know if the changed of the text come from textfield or textFieldSubtitle
            cell.textField?.tag = ViewDisease.itaTfTagTitle
            cell.textFieldSubtitle.tag = ViewDisease.itaTfTagDetail

            if tableColumn == tableView.tableColumns[1]{//Traslate image
                cell.textField?.tag = ViewDisease.translationTfTagTitle
                cell.textFieldSubtitle.tag = ViewDisease.translationTfTagDetail
            }
            //set the delegate to trigger the method on changing text
            cell.textField?.delegate = self
            cell.textFieldSubtitle.delegate = self
            //set editable for cell textField
            cell.textField?.isEditable = true

            var title = ""
            var subtitle = ""
            if let value = obj.dictForLang[language]{
                title = value.name
            }
            
            if let d = obj.description(for: language){
                subtitle = d
            }
            cell.textField?.stringValue = title
            //cell.textFieldSubtitle.stringValue = subtitle
            cell.textFieldSubtitle.attributedStringValue = NSMutableAttributedString(fromTaggedString: subtitle, baseAttributes: [NSAttributedString.Key.font:NSFont.systemFont(ofSize: SubtitleTableCellView.detailFontSize), NSAttributedString.Key.foregroundColor:NSColor.controlTextColor])
            if let imageCell = cell as? SubtitleImageTableCellView{
                imageCell.imageView?.image = nil
                if let image = obj.imageObject?.image{
                    imageCell.imageView?.image = image
                }else if let url = obj.imageObject?.imageUrl{
                    if let imageCell = cell as? SubtitleImageTableCellView{
                        imageCell.progressIndicator.isIndeterminate = false
                        imageCell.progressIndicator.isHidden = false
                        imageCell.progressIndicator.startAnimation(cell)
                        
                        let downloader = ImageDownloader(with: url)
                        downloader.downloadImage(percentDownloadBlock: {(percentDownload) in
                            imageCell.progressIndicator.doubleValue = Double(percentDownload)
                        }, completionBlock: { (image, error, downloaded) in
                            if let img = image{
                                DispatchQueue.main.async {
                                    imageCell.progressIndicator.stopAnimation(imageCell)
                                    imageCell.progressIndicator.isHidden = true
                                    imageCell.imageView?.image = img
                                }
                            }
                        })
                    }
                }
            }
            
            
            return cell
        }
        return nil
    }
    
    override func tableView(_ tableView: NSTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var result = super.tableView(tableView, heightForRowAt: indexPath)//this made a setup in superview

        let obj = self.object(at: indexPath)
        
        let widthColumn = self.tableView.tableColumns[0].width
        var imagePresent = false
        if obj.imageObject != nil{
            imagePresent = true
        }
        result = SubtitleTableCellView.height(for: obj.name(for: "ita"), subTitle: obj.description(for: "ita"), columnWidth: widthColumn, imagePresent: imagePresent)
        if result < 74 && imagePresent{ //imageview height
            result = 74
        }
        return result
        
    }
    
    
    override func tableView(_ tableView: NSTableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndexPath = indexPath
        
    }
    var selectedIpToModify:IndexPath?
    
    override func tableView(_ tableView: NSTableView, didRightClickSelectRowAt indexPath: IndexPath, in rect: CGRect) {
        if indexPath.section > 0{
            self.selectedIpToModify = indexPath
            let storyboard = NSStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateController(withIdentifier: "SetupDataSourceObjectVC") as! SetupDataSourceObjectVC
            //vc.delegate = self
            vc.delegate = self
            vc.showAccessoryData = false
            vc.object = self.object(at: indexPath)
            self.present(vc, asPopoverRelativeTo: rect, of: self.tableView, preferredEdge: NSRectEdge.minY, behavior: NSPopover.Behavior.semitransient)
        }
    }
    
    override func tableView(_ tableView: NSTableView, actionsForRowAtIndexPath indexPath: IndexPath, forEdge edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        if indexPath.section != 0{
            return super.tableView(tableView, actionsForRowAtIndexPath: indexPath, forEdge: edge)
        }
        
        return [NSTableViewRowAction]()
    }
    
    //MARK: ViewerSuperClassProtocol
    override func save(titleText: String, at indexPath: IndexPath, for language: String) {
        let obj = self.object(at: indexPath)
        if var value = obj.dictForLang[language]{
            value.name = titleText
            obj.dictForLang[language] = value
        }else{
            let value:(String, String?) = (titleText, nil)
            obj.dictForLang[language] = value
        }
        self.saveObject(object: obj, at: indexPath)
    }
    
    override func save(detailText: String, at indexPath: IndexPath, for language: String) {
        let obj = self.object(at: indexPath)
        if var value = obj.dictForLang[language]{
            value.description = detailText
            obj.dictForLang[language] = value
        }else{
            let value:(String, String?) = ("No name", detailText)
            obj.dictForLang[language] = value
        }
        self.saveObject(object: obj, at: indexPath)

    }
    
    func saveObject(object:DataSourceObject, at indexPath:IndexPath){
        if self.categorized{
            if indexPath.section == 0{//nsme and description
                self.objectCollection.nameAndDescriptionObj = object
            }else{
                //Do nothing
                //because in the dict mantain the reference to the object in Object collection
                /*
                let objectAtIp = self.object(at: indexPath)
                let index = self.objectCollection.objects.index(where: { (obj) -> Bool in
                    
                })
 */
            }
        }else{
            //self.objectCollection.objects[indexPath.item] = object
        }

    }
    //MARK: SetupDataSourceObjectVCProtocol
    func setupDataSourceObjectVCProtocolDidEnd(with object: DataSourceObject?, setupDataSourceObjectVC: SetupDataSourceObjectVC) {
        //The object has been modified
        if let ip = self.selectedIpToModify{
            self.selectedIpToModify = nil
            self.reloadRows(at: [ip], forColumns: [0, 1])
            //Add image removed to remove image
            if let imageObject = setupDataSourceObjectVC.imageObjectToRemove{
                self.imageObjectsToRemove.append(imageObject)
            }
            //Add object created to added object
            if let obj = object{
                if DataSourceObject.arrayDataSourceObject(contain: self.objectAdded, objectWith: obj.value) == nil{
                    //if modify the object it will be added only if not present
                    self.objectAdded.append(obj)
                }
            }

        }else{//the object i new
            if let obj = object{
                self.objectCollection.objects.append(obj)
                self.resetDataSourceDict(with: self.objectCollection.objects)
                self.objectAdded.append(obj)
                self.tableView.reloadData()
            }
            
        }
        
        self.dismiss(setupDataSourceObjectVC)
    }
    
    func setupDataSourceObjectVCProtocolDidCancel(setupDataSourceObjectVC: SetupDataSourceObjectVC) {
        self.selectedIpToModify = nil
        self.dismiss(setupDataSourceObjectVC)

    }
    //MARK: Saving
    var objectAdded = [DataSourceObject]()
    var imageObjectsToRemove = [ImageObject]()
    var imageObjectAddedSaving:Int?
    var isSaving = false
    
    func save(){
        if let group = self.group{
            if self.isSaving == false{
                self.loadingLabel(loading: true, with: "Loading Data...".localized, color: nil)
            }
            
            self.isSaving = true
            
            //remove image objects to remove
            if self.imageObjectsToRemove.count > 0{
                let imageObject = self.imageObjectsToRemove.removeLast()
                ImageManager.shared.deleteImageObject(imageObject: imageObject, withCompletionBlock: {[weak self] (data, response, error) in
                    var text = "Removed image at url".localized
                    if let stringUrl = imageObject.imageUrl?.absoluteString{
                        text = text + stringUrl
                    }
                    DispatchQueue.main.async {
                        self?.loadingLabel(loading: true, with: text, color: NSColor.orange)
                    }
                    self?.saveAction()


                })
                return
            }
           
            //save images
            if self.imageObjectAddedSaving == nil{
                self.imageObjectAddedSaving = 0
            }
            if self.imageObjectAddedSaving! < self.objectAdded.count{
                let obj = self.objectAdded[self.imageObjectAddedSaving!]
                self.imageObjectAddedSaving = self.imageObjectAddedSaving! + 1
                if let category = DataSource.shared.delegate.selectedCategory(){
                    if let imageObject = obj.imageObject{
                        if imageObject.imageUrl == nil{
                            
                            DispatchQueue.main.async {
                                self.loadingLabel(loading: true, with: "Loading image...".localized, color: nil)
                                self.setPercentProgressionIndicator(percent: 0)
                                
                            }
                            let rootUrl = ImageManager.shared.delegate.rootUrlForImageObject()
                            
                            let imageObjectDirectory = "DataSource/\(category)/BaseData/ImagesGeneralData/"
                            if let imageObjectDelegate = ImageManager.shared.imageObjectDelegate{
                                
                                imageObject.saveRemotely(withDelegate: imageObjectDelegate,
                                                         baseFileName: "image",
                                                         directoryName: imageObjectDirectory,
                                                         rootUrl: rootUrl,
                                                         overwrite: false,
                                                         checkForSubDirectoryName: true,
                                                         percentUploadBlock: { [weak self](percentUpload) in
                                                            DispatchQueue.main.async {
                                                                self?.setPercentProgressionIndicator(percent: percentUpload)}},
                                                         completionImage: { [weak self](imageNumber, sequenceCount) in
                                                            DispatchQueue.main.async {
                                                                let message = "image sequence".localized + " \(imageNumber)/\(sequenceCount)"
                                                                self?.loadingLabel(loading: true, with: message, color: nil)}},
                                                         completionFinal: { [weak self](imageCount) in
                                                            DispatchQueue.main.async {
                                                                let message = "image".localized + " \(imageCount)/\(imageCount)"
                                                                self?.loadingLabel(loading: true, with: message, color: nil)
                                                                self?.saveAction()
                                                            }
                                                            
                                })
                            }else{
                                print("Images not saved because imageObjectDelegate of ImageManager has not be set")
                            }
                        }else{
                            //no image object
                            self.saveAction()
                        }
                    }else{
                        //no image object
                        self.saveAction()
                    }
                }
                
                
                
            }else{
                self.imageObjectAddedSaving = nil
                self.isSaving = false
                self.loadingLabel(loading: true, with: "Saving file...", color: nil)
                DataSourceRadiology.shared.addGeneralDataCollection(with: self.objectCollection, groupName: group, completion: { [weak self](success, error) in
                    DispatchQueue.main.async {
                        self?.unsavedChange = false
                        self?.showSaveMessage(for: success, error: error)
                        self?.sendSaveNotification()

                    }
                })
                
                
            }
            
        }else{
            print("Attenzione: nessun group name ")
        }
        
        
    }

    //MARK:Translating
    func translateRow(){
        if let ip = self.selectedIndexPath{
            self.loadingLabel(loading: true, with: "Traslating...", color: nil)
            
            let fromTranslationLanguage = "ita"
            let toTranslationLanguage = self.translationLanguage
            
            let obj = self.object(at: ip)
            
            if let detail = obj.description(for: fromTranslationLanguage){
                
                GoogleTranslateManager.translateToEnglish(text: detail, completionHandler: { [weak self] (text, error) in
                    var success = true
                    if error != nil{
                        success = false
                    }
                    self?.showTranslateMessage(for: success, error: error)
                    if let tableView = self?.tableView{
                        if let row = self?.rowNumber(for: ip, in: tableView), let t = text, let columnId = self?.tableView.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue:"TranslationColumnId")){
                            let correctedText = MyXMLNode.cleanedStringFromXML(string: t)
                            self?.save(detailText: correctedText!, at: ip, for: toTranslationLanguage)
                            
                            //self?.tableView.reloadData()
                            self?.tableView.reloadData(forRowIndexes: IndexSet.init(integer:row), columnIndexes: IndexSet.init(integer: columnId))
                            self?.selectedIndexPath = nil
                        }
                    }
                    
                })
            }else{
                self.loadingLabel(loading: false, with: "Nothing to translate", color: NSColor.gray)
            }
            
        }
    }
}
