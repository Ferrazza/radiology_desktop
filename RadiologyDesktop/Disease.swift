//
//  Disease.swift
//
//  Created by Alessandro Ferrazza on 23/01/16.
//  Copyright © 2016 Alessandro Ferrazza. All rights reserved.
//

import MacBaseKit
import SharedFilesForMac

/*
class Element {
    
    var name:String
    var elementDescription:String?
    var images = [ImageObject]()
    var value:Float?
    var excludeIfPresent:Bool?
    var excludeIfAbsent:Bool?
    var excludeIfAllOfThemAreAbsent:Bool?

    init (name:String, description:String?, imageUrls:[URL]?){
        self.name = name
        self.elementDescription = description
        if let urls = imageUrls{
            for url in urls{
                self.addImageObject(withImageUrl: url)
            }
        }
        
    }
    
    func addImageObject(withImageUrl imageUrl:URL){
        let imageObject = ImageObject(title: self.name, description: self.elementDescription, imageUrl: imageUrl)
        self.images.append(imageObject)
    }

    func addImageObject(withImage image:NSImage){
        let imageObject = ImageObject(title: self.name, description: self.elementDescription, image: image)
        self.images.append(imageObject)
    }

    func addImageObjIfNeeded(_ imageObj:ImageObject){
        if !self.containImageObj(withSameUrlOf: imageObj){
            self.images.append(imageObj)
        }
    }
    
    func containImageObj(withSameUrlOf imageObj:ImageObject)->Bool{
        var result = false
        for imgObj in self.images{
            if let myUrl = imgObj.imageUrl, let checkedUrl = imageObj.imageUrl{
                if myUrl == checkedUrl{
                    result = true
                }
            }
        }
        return result
    }
    var imageObjectsPreparedToRemove = [ImageObject]()
    
    func removeUrlPrepared(){
        for obj in self.imageObjectsPreparedToRemove{
            ImageManager.shared.deleteImageObject(imageObject: obj, withCompletionBlock: nil)
        }
        self.imageObjectsPreparedToRemove.removeAll()
    }
    func prepareToRemoveImageObjWithUrl(_ imageUrl:URL){
        
        var index:Int?
        for (i, imgObj) in self.images.enumerated(){
            if let myUrl = imgObj.imageUrl{
                if myUrl.absoluteString == imageUrl.absoluteString{
                    self.imageObjectsPreparedToRemove.append(imgObj)
                    index = i
                }
            }
        }
        if let i = index{
            self.images.remove(at: i)
        }
    }

    init (){
        self.name = ""
    }
    
    //MARK: helper
    func getNameAndDataString(from dictOfDataSourceObjects:[String:DataSourceObject]?)->String{
        var name = self.name
        var value:Float = 1
        if let dict = dictOfDataSourceObjects{
            if let obj = dict[self.name]{
                name = obj.name
            }
        }
        
        if let v = self.value{
            value = v
        }
        var excludeText = ""
        if let exclude = self.excludeIfPresent{
            if exclude{
                excludeText = "Exclude if present".localized
            }
        }
        if let exclude = self.excludeIfAbsent{
            if exclude{
                excludeText = "Exclude if absent".localized
            }
        }
        if let exclude = self.excludeIfAllOfThemAreAbsent{
            if exclude{
                excludeText = "Exclude if all of them are absent".localized
            }
        }
        var text = name + " (\(value)"
        if excludeText.count > 0{
            text = text + " - \(excludeText))"
        }else{
            text = text + ")"
        }
        return text
    }
    static func arrayOfElementWithoutExcluded(_ elements:[Element]) -> (array:[Element], excluded:[Element]){
        var result = [Element]()
        var excluded = [Element]()

        for element in elements{
            var add = true
            if let e = element.excludeIfPresent {
                if e == true{
                    add = false
                    excluded.append(element)
                }
            }
            if add{
                result.append(element)
            }
        }
        return (result, excluded)
    }
    
    static func dictElementForCategory(from array:[Element], baseData:BaseData) -> [String:[Element]]{
        var dict = [String:[Element]]()
        for element in array{
            //get category
            var category = "Others".localized
            if let obj = baseData.object(with: element.name){
                if let cat = obj.category{
                    category = cat
                }
            }
            var array = dict[category]
            if array == nil{
                array = [Element]()
            }
            array?.append(element)
            dict[category] = array!
        }
        return dict
    }

    //MARK: Generating XML
    func generateXmlElement()->MyXMLElement{
        var attr = [Disease.nameKey:self.name]
        if let value = self.value{
            attr[Disease.valueKey] = String(value)
        }
        if let excludeIfAbsent = self.excludeIfAbsent{
            attr[Disease.excludeIfAbsent] = String(excludeIfAbsent)
        }
        if let excludeIfAllOfThemAreAbsent = self.excludeIfAllOfThemAreAbsent{
            attr[Disease.excludeIfAllOfThemAreAbsent] = String(excludeIfAllOfThemAreAbsent)
        }
        if let excludeIfPresent = self.excludeIfPresent{
            attr[Disease.excludeIfPresent] = String(excludeIfPresent)
        }
        let signElem = MyXMLElement(name: Disease.signKey, attributes: attr , value: nil)
        let imageXML = Disease.createImagesXmlElement(self.name, images: self.images)
        for child in imageXML.childs{//add single child and not image tag
            signElem.add(child: child)
        }
        return signElem
    }
}



*/

class Disease: NSObject {
    //Adding info
    
    var actualLanguage = "eng"

    var diseaseIdentifier:String?

    var diseaseFree = true
    var diseaseImageComplete = true

    var diseaseName:String
    var diseaseFrequency:DataSourceObjectFrequency
    var diseaseOnset:String?
    var diseaseImages = [ImageObject]()
    var diseaseReferences = [PMReference]()

    var diseaseDescription:String?
    var xmlElement:MyXMLElement?
    
    
    var diseaseMaleFemaleRatio:MaleFemaleRatio?
    var diseaseAgeRanges = [AgeRange]()
    var diseaseSigns = [String:[Element]]()//are the signs in a dict where the key is the name of the group
    var diseasebaseRadiologySigns = [Element]()//an array of base radiology signs
    var diseaseOtherData = [Element]()//an array of base radiology signs

    
    //MARK: helpers
    func removeSign(with name:String, inGroup group:String){
        if var arraySigns = self.diseaseSigns[group]{
            var indexToRemove:Int?
            for (index, sign) in arraySigns.enumerated(){
                if sign.name == name{
                    indexToRemove = index
                    break
                }
            }
            if let index = indexToRemove{
                arraySigns.remove(at: index)
                self.diseaseSigns[group] = arraySigns
            }
        }
        
    }
    
    func replaceSign(signToReplace:Element, inGroup group:String)->Bool{
        if var arraySigns = self.diseaseSigns[group]{
            var indexToReplace:Int?
            for (index, sign) in arraySigns.enumerated(){
                if sign.name == signToReplace.name{
                    indexToReplace = index
                    break
                }
            }
            if let index = indexToReplace{
                arraySigns[index] = signToReplace
                self.diseaseSigns[group] = arraySigns
                return true
            }
        }
        return false
        
    }
    

    static func containsSignWithNameInArray(_ name:String, arraySignObj:[Element])->(contain:Bool, value:Float?){
        var contain = false
        var value:Float? = nil
        for sign in arraySignObj{
            if sign.name == name{
                contain = true
                value = sign.value
            }
        }
        return (contain, value)
    }
    
    static func mustBeExcludedForSignsNamesAndArraySignObject(_ providedSigns:[String], arraySignObj:[Element])->Bool{
        //return yes if the sign must be excluded because of excludeIfPresent or excludeIfAbsent is present
        var excludeForAllOfThemAbsent = false
        var first_excludeForAllOfThemAbsent_evaluated = false
        for sign in arraySignObj{
            //At the first sign containing the tag excludeIfAllOfThemAreAbsent the bool excludeForAllOfThemAbsent become true.
            if first_excludeForAllOfThemAbsent_evaluated == false{
                if sign.excludeIfAllOfThemAreAbsent == true{
                    first_excludeForAllOfThemAbsent_evaluated = true
                    excludeForAllOfThemAbsent = true
                }
            }
            //evaluating excluseIfPresent and excludeIfAbsent
            var isSignPresent = false
            for providedSignName in providedSigns{
                if sign.name == providedSignName{
                    isSignPresent = true
                    if sign.excludeIfPresent == true{
                        return true
                    }
                }
            }
            if isSignPresent == false && sign.excludeIfAbsent == true{
                return true
            }
            
            //then the tag excludeForAllOfThemAbsent become false (do not exclude) if at least one of them is present
            if isSignPresent == true && sign.excludeIfAllOfThemAreAbsent == true{
                excludeForAllOfThemAbsent = false
            }
            
            
        }
        if excludeForAllOfThemAbsent == true{
            return true
        }
        return false
    }

        
    static func getImageDescriptionsFromImages(_ images:[ImageObject])->[String:(title:String, description:String)]{
        var result = [String:(title:String, description:String)]()
        for image in images{
            if let imageUrl = image.imageUrl, let imageDescription = image.imageDescription{
                result[imageUrl.absoluteString] = (title:image.imageTitle, description: imageDescription)
            }
        }
        return result
    }
    static func setImagesWithImageDescriptions(_ diseaseName:String, imageDescriptionRescue:String?, images:[ImageObject], imageDescritpions:[String:(title:String, description:String)])->[ImageObject]{
        var result = [ImageObject]()
        for imageObj in images{
            //remove actual values
            imageObj.imageDescription = imageDescriptionRescue
            imageObj.imageTitle = diseaseName
            for (key, value) in imageDescritpions{
                if let imageUrl = imageObj.imageUrlStringLastPaths{
                    if key == imageUrl{
                        imageObj.imageTitle = value.title
                        imageObj.imageDescription = value.description
                    }
                }
            }
            result.append(imageObj)
        }
        return result
    }
    
    //With this can access the properties with subscription
    subscript (propertyName:String)->AnyObject?{
        get{
            if let propertyValue = self.value(forKey: propertyName){
                return propertyValue as AnyObject?
            }
            return nil
        }
        set{
            self.setValue(newValue, forKey: propertyName)
        }
    }
    
    var attributedDescription:NSAttributedString{
        get{
            let dictTitle = [NSAttributedString.Key.font:NSFont.boldSystemFont(ofSize: 18),
                NSAttributedString.Key.foregroundColor:NSColor.black]
            let dictText = [NSAttributedString.Key.font:NSFont.systemFont(ofSize: 14),
                NSAttributedString.Key.foregroundColor:NSColor.black]
            let att = NSMutableAttributedString()
            if let description = self.diseaseDescription{
                att.append(NSMutableAttributedString(string: "Description:\n", attributes: dictTitle))
                att.append(NSMutableAttributedString(string: description, attributes: dictText))
            }
            return att.copy() as! NSAttributedString
        }
    }
    
    
    func dictOtherSignsForOtherLanguage(for signGroup:String)->[String:[Element]]{
        var dict = [String:[Element]]()
        for lan in self.availableLanguages(){
            if lan != self.actualLanguage, let e = xmlElement{
                let tuple = Disease.loadElement(for: lan, xmlElement: e)
                let signs = tuple.signs[signGroup]
                dict[lan] = signs
            }
        }
        return dict
    }
    
 
    
    func availableLanguages()->[String]{
        var array = [String]()
        if self.xmlElement != nil{
            if let dataChilds = self.xmlElement?.getChilds(with: SharedKeys.dataKey){
                for child in dataChilds{
                    if let lan = child.attributes[SharedKeys.languageKey]{
                        array.append(lan)
                    }
                }
            }
        }
        return array
        
    }

    func removeAllImagesInFileManager(){
        for imgObj in self.diseaseImages{
            if let myUrl = imgObj.imageUrl{
                if imgObj.isSequence{//is sequence
                    if let baseUrl = imgObj.imageSequenceBaseUrl{
                        
                        //remove last path component
                        let folderUrl = baseUrl.stringByDeletingLastPathComponent
                        //then remove the folder in remote
                        ImageManager.shared.deleteFileFromFileManager(with: folderUrl, isSequence: true)
                    }
                }else{
                    ImageManager.shared.deleteFileFromFileManager(with: myUrl.absoluteString, isSequence: false)
                }
            }
        }
    }
    var imageObjectsPreparedToRemove = [ImageObject]()
    
    func removeUrlPrepared(){
        for obj in self.imageObjectsPreparedToRemove{
            ImageManager.shared.deleteImageObject(imageObject: obj, withCompletionBlock: nil)
        }
        self.imageObjectsPreparedToRemove.removeAll()
    }
    func prepareToRemoveImageObjWithUrl(_ imageUrl:URL){
        var index:Int?
        for (i, imgObj) in self.diseaseImages.enumerated(){
            if let myUrl = imgObj.imageUrl{
                if myUrl.absoluteString == imageUrl.absoluteString{
                    self.imageObjectsPreparedToRemove.append(imgObj)
                    index = i
                }
            }
        }
        if let i = index{
            self.diseaseImages.remove(at: i)
        }
    }


    
    func reloadElementForLanguage(_ language:String){
        if let e = self.xmlElement{
            let values = Disease.loadElement(for: language, xmlElement:e)
            self.diseaseName = values.name
            
            self.diseaseFrequency = values.frequency
            self.diseaseOnset = values.diseaseOnset
            self.diseaseDescription = values.description
            self.diseaseReferences = values.references
            //self.diseaseSmokeRelated = values.smokeRelated
            self.diseaseMaleFemaleRatio = values.maleFemaleRatio
            self.diseaseAgeRanges = values.ageRanges
            self.diseaseFree = values.isFree
            self.diseaseImageComplete = values.isImageComplete
            
            //setupSigns
            self.setupSignsDictAndBaseRadiologySigns(from: values.signs)

            if let lan = values.loadedLanguage{
                self.actualLanguage = lan
            }
            
            //self.diseaseImageDescriptions = values.imageDescriptions
            self.diseaseImages = Disease.setImagesWithImageDescriptions(self.diseaseName, imageDescriptionRescue:nil, images:self.diseaseImages, imageDescritpions:values.imageDescriptions)
        }
     

    }
    
    
    init(xmlElement:MyXMLElement, identifier:String){
        self.xmlElement = xmlElement
        
        self.diseaseIdentifier = identifier
        
        let values = Disease.loadElement(for: SharedKeys.languageKey.localized, xmlElement:xmlElement)
        self.diseaseName = values.name

        self.diseaseFrequency = values.frequency
        self.diseaseOnset = values.diseaseOnset
        self.diseaseDescription = values.description
        self.diseaseReferences = values.references
        //self.diseaseSmokeRelated = values.smokeRelated
        self.diseaseMaleFemaleRatio = values.maleFemaleRatio
        self.diseaseAgeRanges = values.ageRanges
        
        self.diseaseFree = values.isFree
        self.diseaseImageComplete = values.isImageComplete

        

        if let lan = values.loadedLanguage{
            self.actualLanguage = lan
        }
        //Images
        let images = Disease.loadImagesFromXML(xmlElement)
        self.diseaseImages = Disease.setImagesWithImageDescriptions(self.diseaseName, imageDescriptionRescue:nil, images:images, imageDescritpions:values.imageDescriptions)
        
        
        
        super.init()
        
        //setupSigns
        self.setupSignsDictAndBaseRadiologySigns(from: values.signs)
    }
    
    init(language:String){
        self.diseaseName = ""
        self.actualLanguage = language
        self.diseaseFrequency = DataSourceObjectFrequency.common
        super.init()
        self.newXmlElementForActualLanguage()

        
        
    }
    
    
    func saveXmlElementForActualLanguage(){
        //find the child with language
        if let element = self.xmlElement{
            
            var indexCommon:Int?
            for (i, child) in element.childs.enumerated(){
                if let c = child as? MyXMLElement{
                    if c.name == SharedKeys.commonKey{
                        indexCommon = i
                    }
                }
            }
            let newCommonElement = Disease.createCommonXmlElement(with: self.diseaseFrequency, diseaseOnset: self.diseaseOnset, signs: self.allSignsDict(), references: self.diseaseReferences, isFree: self.diseaseFree, isImageComplete: self.diseaseImageComplete, maleFemaleRatio: self.diseaseMaleFemaleRatio, ageRanges: self.diseaseAgeRanges)
            if let i = indexCommon{
                element.childs.remove(at: i)
                element.childs.insert(newCommonElement, at: i)
            }else{
                element.childs.append(newCommonElement)
            }
            
            var indexImage:Int?
            for (i, child) in element.childs.enumerated(){
                if let c = child as? MyXMLElement{
                    if c.name == SharedKeys.imagesKey{
                        indexImage = i
                    }
                }
            }
            let newImageElement = Disease.createImagesXmlElement(self.diseaseName, images: self.diseaseImages)
            if let i = indexImage{
                element.childs.remove(at: i)
                element.childs.insert(newImageElement, at: i)
            }else{
                element.childs.append(newImageElement)
            }
            
            var indexData:Int?
            for (i, child) in element.childs.enumerated(){
                if let c = child as? MyXMLElement{
                    if c.name == SharedKeys.dataKey{
                        if let lan = c.attributes[SharedKeys.languageKey]{
                            if lan == self.actualLanguage{
                                indexData = i
                            }
                        }
                    }
                }
            }
            
            let newDataElement = Disease.createDataXmlElement(for: self.actualLanguage, name: self.diseaseName, description: self.diseaseDescription, signs: self.allSignsDict(), imageDescriptions: Disease.getImageDescriptionsFromImages(self.diseaseImages))
            if let i = indexData{
                element.childs[i] = newDataElement
            }else{
                element.childs.append(newDataElement)
            }
        }

    }

    func newXmlElementForActualLanguage(){
        //find the child with language
        let baseElement = MyXMLElement(name: "root", value: nil)
        let newCommonElement = Disease.createCommonXmlElement(with: self.diseaseFrequency, diseaseOnset: self.diseaseOnset, signs:self.allSignsDict(), references: self.diseaseReferences, isFree: self.diseaseFree, isImageComplete: self.diseaseImageComplete, maleFemaleRatio: self.diseaseMaleFemaleRatio, ageRanges: self.diseaseAgeRanges)
        baseElement.add(child: newCommonElement)

        let newDataElement = Disease.createDataXmlElement(for: self.actualLanguage, name: self.diseaseName, description: self.diseaseDescription, signs:self.allSignsDict(), imageDescriptions: Disease.getImageDescriptionsFromImages(self.diseaseImages))
        baseElement.add(child: newDataElement)
    
        let newImageElement = Disease.createImagesXmlElement(self.diseaseName, images: self.diseaseImages)
        baseElement.add(child: newImageElement)
        self.xmlElement = baseElement
        
    }


    static func generateFileNameForDisease(_ disease:Disease)->String{
        let actualLan = disease.actualLanguage
        disease.reloadElementForLanguage("ita")
        let filename = disease.diseaseName.replacingOccurrences(of: " ", with: "_")
        disease.reloadElementForLanguage(actualLan)
        return filename
    }
    
    func save(in group:String, category:String, completion: @escaping (_ success: Bool, _ error: Error?) -> Void){
        self.saveXmlElementForActualLanguage()
        //correct the name by removing spaces
        let diseaseName = Disease.generateFileNameForDisease(self)
        //send xmlDocument
        if let element = self.xmlElement{
            DataSourceRadiology.saveXMLElement(with: self.diseaseIdentifier, group:group, category:category, diseaseName:diseaseName, element: element) { (success, identifier, error) -> Void in
                if let i = identifier{
                    self.diseaseIdentifier = identifier
                    print("identifier saved disease = \(i)")
                }else{
                    print("error in saving xml element with error: \(String(describing: error?.localizedDescription))")
                }
                completion(success, error)
            }
        }
        
        
    }
    
        
    static func getAttributeValueInElement(_ element:MyXMLElement, childName:String, attributeName:String)->String?{
        var attributeValue:String?
        
        if let child = element.getChild(with: childName){
            if let value = child.attributes[attributeName]{
                attributeValue = value
            }
        }
        return attributeValue
    }
    
    static func getAttributeValuesInElement(_ element:MyXMLElement, childName:String, attributeName:String)->[String]{
        var attributeValues = [String]()
        for child in element.getChilds(with: childName){
            if let value = child.attributes[attributeName]{
                attributeValues.append(value)
            }
        }
        return attributeValues
    }
    
    
   
   
    
    //------------------------------------------------------
    //------------------------------------------------------
    //------------------------------------------------------
    //MARK: XML keys
    //------------------------------------------------------

    

    //------------------------------------------------------
    //------------------------------------------------------
    //------------------------------------------------------
    //MARK: Generating XML components from disease
    //------------------------------------------------------
    
    func allSignsDict()->[String:[Element]]{
        var result = [String:[Element]]()
        for (key, signs) in self.diseaseSigns{
            result[key] = signs
        }
        if self.diseasebaseRadiologySigns.count > 0{
            result[DataSourceRadiology.baseRadiologySignsKey] = self.diseasebaseRadiologySigns
        }
        if self.diseaseOtherData.count > 0{
            result[DataSourceRadiology.otherDataKey] = self.diseaseOtherData
        }
        return result
    }
    
    static func createCommonXmlElement(with frequency:DataSourceObjectFrequency?, diseaseOnset:String?, signs:[String:[Element]], references:[PMReference]?, isFree:Bool, isImageComplete:Bool, maleFemaleRatio:MaleFemaleRatio?, ageRanges:[AgeRange])->MyXMLElement{
        
        
        let mainElement = MyXMLElement(name: SharedKeys.commonKey, value:nil)

        var frequencyNumber = String(DataSourceObjectFrequency.common.rawValue)
        if let f = frequency{
            frequencyNumber = String(f.rawValue)
        }
        let frequencyElement = MyXMLElement(name: SharedKeys.frequencyKey, attributes: [SharedKeys.valueKey:"\(frequencyNumber)"], value: nil)
        mainElement.add(child: frequencyElement)

        
       
        let freeElement = MyXMLElement(name: SharedKeys.isFreeKey, attributes: [SharedKeys.valueKey:"\(isFree)"], value: nil)
        mainElement.add(child: freeElement)

        let imageCompleteElement = MyXMLElement(name: SharedKeys.isImageCompleteKey, attributes: [SharedKeys.valueKey:"\(isImageComplete)"], value: nil)
        mainElement.add(child: imageCompleteElement)

        if let mFRatio = maleFemaleRatio{
            var attr = [SharedKeys.maleKey:"\(mFRatio.male)"]
            attr[SharedKeys.femaleKey] = "\(mFRatio.female)"
            let maleFemaleElement = MyXMLElement(name: SharedKeys.maleFemaleRatioKey, attributes: attr, value: nil)
            mainElement.add(child: maleFemaleElement)
        }
        
        
        if ageRanges.count > 0{
            let ageRangesElem = MyXMLElement(name:SharedKeys.ageRangesKey)
            for ageRange in ageRanges{
                var attr = [SharedKeys.minKey:"\(ageRange.min)"]
                attr[SharedKeys.maxKey] = "\(ageRange.max)"
                let ageRangeElem = MyXMLElement(name: SharedKeys.itemKey, attributes: attr , value: nil)
                ageRangesElem.add(child:ageRangeElem)
            }
            mainElement.add(child: ageRangesElem)
        }
        
        if let o = diseaseOnset{
            let diseaseOnsetElem = MyXMLElement(name: DataSourceRadiology.diseaseOnsetKey, attributes: [SharedKeys.valueKey:o], value: nil)
            mainElement.add(child: diseaseOnsetElem)
        }
        
        if signs.count > 0 {
            let signsMainElement = MyXMLElement(name:DataSourceRadiology.signsKey)
            for (groupName, arraySigns) in signs{
                let signsGroupElement = MyXMLElement(name:groupName)
                for sign in arraySigns{
                    let signElement = sign.generateXmlElement()
                    signsGroupElement.add(child: signElement)
                }
                signsMainElement.add(child: signsGroupElement)
            }
            
            mainElement.add(child: signsMainElement)
        }
        if let refs = references{
            let refElement = self.createReferenceXmlElement(refs)
            mainElement.add(child: refElement)
        }
        

        return mainElement
    }
    
    static func createDataXmlElement(for language:String, name:String, description:String?, signs:[String:[Element]], imageDescriptions:[String:(title:String, description:String)])->MyXMLElement{
        
        
        let mainElement = MyXMLElement(name: SharedKeys.dataKey)
        mainElement.attributes[SharedKeys.nameKey] = name
        mainElement.attributes[SharedKeys.languageKey] = language
        
        if signs.count > 0{
            let signsMainElement = MyXMLElement(name:DataSourceRadiology.signsKey)
            for (groupName, arraySigns) in signs{
                let signsGroupElement = MyXMLElement(name:groupName)
                for sign in arraySigns{
                    var attr = [SharedKeys.nameKey:sign.name]
                    if let d = sign.elementDescription{
                        if d.count > 0{
                            attr[SharedKeys.descriptionKey] = d
                        }
                    }
                    let signElem = MyXMLElement(name: SharedKeys.signKey, attributes: attr , value: nil)
                    signsGroupElement.add(child: signElem)
                }
                signsMainElement.add(child: signsGroupElement)
            }
            mainElement.add(child: signsMainElement)
            
        }
        
        if let d = description{
            let descriptionElem = MyXMLElement(name: SharedKeys.diseaseDescriptionKey, attributes: [SharedKeys.valueKey:d], value: nil)
            mainElement.add(child: descriptionElem)
        }
        
        
        let imageDescriptionsElem = MyXMLElement(name: SharedKeys.imageDescriptionsKey)

        for (key, value) in imageDescriptions{
            let attr = [SharedKeys.imageUrlKey:key,
                        SharedKeys.titleKey:value.title,
                        SharedKeys.descriptionKey:value.description]
            let itemElem = MyXMLElement(name: SharedKeys.itemKey, attributes: attr , value: nil)
            imageDescriptionsElem.add(child: itemElem)
        }
        mainElement.add(child: imageDescriptionsElem)
        
        return mainElement
    }
    
    static func createImagesXmlElement(_ name:String, images:[ImageObject])->MyXMLElement{
        
        let imageElements = MyXMLElement(name:SharedKeys.imagesKey)
        if images.count > 0{
            for image in images{
                let imageElem = image.generateXmlElement(withName: SharedKeys.itemKey)
                imageElements.add(child: imageElem)
            }
        }
        return imageElements
    }
    
    static func createReferenceXmlElement(_ references:[PMReference])->MyXMLElement{
        let mainElement = MyXMLElement(name: SharedKeys.referencesKey)
        
        for ref in Array(references){
            let item = MyXMLElement(name: SharedKeys.itemKey)
            var dict = [String:String]()
            dict[SharedKeys.PMIDKey] = ref.PMID
            dict[SharedKeys.titleKey] = ref.title
            if let authors = ref.authors{
                dict[SharedKeys.authorsKey] = authors
            }
            if let journal = ref.journal{
                dict[SharedKeys.journalKey] = journal
            }
            item.attributes = dict
            mainElement.add(child: item)
        }
        return mainElement
    }

    //------------------------------------------------------
    //------------------------------------------------------
    //------------------------------------------------------
    //MARK: Generating components from XML
    //------------------------------------------------------
    func setupSignsDictAndBaseRadiologySigns(from allSignsDict:[String:[Element]]) {
        for (key, signs) in allSignsDict{
            if key == DataSourceRadiology.baseRadiologySignsKey{
                self.diseasebaseRadiologySigns = signs
            }else if key == DataSourceRadiology.otherDataKey{
                self.diseaseOtherData = signs
            }else{
                self.diseaseSigns[key] = signs
            }
        }
    }
    static func loadElement(for language:String, xmlElement:MyXMLElement) -> (name:String, frequency:DataSourceObjectFrequency, diseaseOnset:String?, description:String?, signs:[String:[Element]], loadedLanguage:String?, references:[PMReference], isFree:Bool, isImageComplete:Bool, imageDescriptions:[String:(title:String, description:String)], maleFemaleRatio:MaleFemaleRatio?, ageRanges:[AgeRange]){
        
        var name = "no name"
        var frequencyMain = DataSourceObjectFrequency.common
        var diseaseOnset:String?
        var description:String?
        var signs = [String:[Element]]()
        var references = [PMReference]()
        var loadedLanguage:String?
        var imageDescriptions = [String:(title:String, description:String)]()
        
        var isFree = true
        var isImageComplete = false

        let attributeName = SharedKeys.valueKey
        var maleFemaleRatio:MaleFemaleRatio?
        var ageRanges = [AgeRange]()
        
        
        
        
        //load image descriptions
        imageDescriptions = Disease.imageDescriptionForLanguage(xmlElement, language: language);
        
        //select common features from common element
        if let commonChild = xmlElement.getChild(with: SharedKeys.commonKey){
            
            if let f = Disease.getAttributeValueInElement(commonChild, childName: SharedKeys.frequencyKey, attributeName: attributeName){
                if let fInt = Int(f){
                    if let frequency = DataSourceObjectFrequency(rawValue: fInt){
                        frequencyMain = frequency
                    }
                }
            }
            
            diseaseOnset = Disease.getAttributeValueInElement(commonChild, childName: DataSourceRadiology.diseaseOnsetKey, attributeName: attributeName)
            
            //load isFree
            if let free = Disease.getAttributeValueInElement(commonChild, childName: SharedKeys.isFreeKey, attributeName: attributeName){
                isFree = free.boolValue
            }
            //load isImageComplete
            if let imageComplete = Disease.getAttributeValueInElement(commonChild, childName: SharedKeys.isImageCompleteKey, attributeName: attributeName){
                isImageComplete = imageComplete.boolValue
            }
            
            //load maleToFemaleRatio
            if let childMFRatio = commonChild.getChild(with: SharedKeys.maleFemaleRatioKey){
                if let male = childMFRatio.attributes[SharedKeys.maleKey]{
                    if let female = childMFRatio.attributes[SharedKeys.femaleKey]{
                        maleFemaleRatio = MaleFemaleRatio(male: male.intValue, female:female.intValue)
                    }
                }
            }
            //load maleAgeRangesi
            if let childAgeRanges = commonChild.getChild(with: SharedKeys.ageRangesKey){
                for child in childAgeRanges.childs{
                    if let c = child as? MyXMLElement{
                        if let min = c.attributes[SharedKeys.minKey]{
                            if let max = c.attributes[SharedKeys.maxKey]{
                                let range = AgeRange(min: min.intValue, max: max.intValue)
                                ageRanges.append(range)
                            }
                        }
                    }
                }
            }
            
            
            if let childSignsElement = commonChild.getChild(with: DataSourceRadiology.signsKey){
                if let groupSigns = childSignsElement.childs as? [MyXMLElement]{
                    for groupSign in groupSigns{
                        let groupName = groupSign.name
                        if let xmlSigns = groupSign.childs as? [MyXMLElement]{
                            for xmlSign in xmlSigns{
                                let sign = Element(xmlElement:xmlSign)
                                sign.images = Disease.setImagesWithImageDescriptions(sign.name, imageDescriptionRescue:sign.elementDescription, images:sign.images, imageDescritpions:imageDescriptions)
                                var arrayElements = signs[groupName]
                                if arrayElements == nil{
                                    arrayElements = [Element]()
                                }
                                arrayElements!.append(sign)
                                signs[groupName] = arrayElements!
                                
                            }
                        }
                    }
                }
            }
        
            
            if let refsChild = commonChild.getChild(with: SharedKeys.referencesKey){
                for refElement in refsChild.childs{
                    let PMref = PMReference(attributedDict: refElement.attributes)
                    references.append(PMref)
                }
            }
            
        }
        
        //select the language from data element
        let dataChilds = xmlElement.getChilds(with: SharedKeys.dataKey)
        if dataChilds.count > 0{
            var dataElementLanguage:MyXMLElement?
            var dataElementOtherLanguage:MyXMLElement?
            if dataChilds.count > 0{
                dataElementOtherLanguage = dataChilds[0]//it is utilized only if there are no data on specified language
            }
            for child in dataChilds{
                if let lang = child.attributes[SharedKeys.languageKey]{
                    if lang == language{
                        loadedLanguage = language
                        dataElementLanguage = child
                        break
                    }
                }
            }
            
            if let dataElement = dataElementLanguage{
                if let n = dataElement.attributes[SharedKeys.nameKey]{
                    name = n
                }
                
                if let d =  Disease.getAttributeValueInElement(dataElement, childName: SharedKeys.diseaseDescriptionKey, attributeName: attributeName){
                    description = MyXMLNode.reformatNFormat(from:d)
                }
                
                if let childSignsElement = dataElement.getChild(with: DataSourceRadiology.signsKey){
                    if let groupSignsXml = childSignsElement.childs as? [MyXMLElement]{
                        for groupSignXml in groupSignsXml{
                            let groupNameXml = groupSignXml.name
                            if let xmlSigns = groupSignXml.childs as? [MyXMLElement]{
                                for xmlSign in xmlSigns{
                                    if let nameXmlSign = xmlSign.attributes[SharedKeys.nameKey]{
                                        if let groupElement = signs[groupNameXml]{
                                            for element in groupElement{
                                                if element.name == nameXmlSign{
                                                    if let description = xmlSign.attributes[SharedKeys.descriptionKey]{
                                                        element.elementDescription = MyXMLNode.reformatNFormat(from: description)
                                                    }
                                                    break
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }

                
            }else if let dataElement = dataElementOtherLanguage{
                if let n = dataElement.attributes[SharedKeys.nameKey]{
                    name = n
                }
                
                loadedLanguage = language
            }
            
        }
        
        
        return (name, frequencyMain, diseaseOnset, description, signs, loadedLanguage, references, isFree, isImageComplete, imageDescriptions, maleFemaleRatio, ageRanges)
        
    }
    
    static func loadImagesFromXML(_ xmlElement:MyXMLElement)->[ImageObject]{
        //Images
        var result = [ImageObject]()
        if let childImages = xmlElement.getChild(with: SharedKeys.imagesKey){
            result = loadImagesFromXMLContainer(childImages)
        }
        return result

    }

    static func loadImagesFromXMLContainer(_ xmlElementContainer:MyXMLElement)->[ImageObject]{
        return Disease.loadImagesFromXMLContainer(xmlElementContainer, title:nil, description: nil)
    }
    static func loadImagesFromXMLContainer(_ xmlElementContainer:MyXMLElement, title:String?, description:String?)->[ImageObject]{
        //Images
        var result = [ImageObject]()
        for childItem in xmlElementContainer.getChilds(with: SharedKeys.itemKey){
            //Old, now they are all image sequences
            if let imageObject = ImageObject.imageObject(for: childItem, title: title, description: description, rootUrlForImageObject: ImageManager.shared.delegate.rootUrlForImageObject()){
                result.append(imageObject)

            }
        }
        return result
        
    }



    
    static func imageDescriptionForLanguage(_ xmlElement:MyXMLElement, language:String)->[String:(title:String, description:String)]{
        var result = [String:(title:String, description:String)]();
        if let dataChild = xmlElement.getChild(with: SharedKeys.dataKey, attributeName: SharedKeys.languageKey, attributeValue: language){
            if let imageDescriptionsElement = dataChild.getChild(with: SharedKeys.imageDescriptionsKey){
                for iDescriptionChild in imageDescriptionsElement.getChilds(with: SharedKeys.itemKey){
                    if let imageUrl = iDescriptionChild.attributes[SharedKeys.imageUrlKey],
                        let imageTitle = iDescriptionChild.attributes[SharedKeys.titleKey],
                        let imageDescription = iDescriptionChild.attributes[SharedKeys.descriptionKey]{
                        
                        let formattedDescription =  MyXMLNode.reformatNFormat(from: imageDescription)
                        let finalKeyUrl = DataSource.shared.removeApplicationPath(for: imageUrl)
                        result[finalKeyUrl] = (title:imageTitle, description:formattedDescription)
                    }
                }
            }
        }
        return result
    }
    

    
    static func setUncommonPatternDescriptionInArray(_ array:[(pattern:String, subPattern:String, patternDescription:String?, frequency:DataSourceObjectFrequency)], pattern:String, subPattern:String, description:String)->[(pattern:String, subPattern:String, patternDescription:String?, frequency:DataSourceObjectFrequency)]{
        var correctedArray = array
        for (index, var value) in array.enumerated(){
            if value.pattern == pattern && value.subPattern == subPattern{
                value.patternDescription = description
                correctedArray[index] = value
                break
            }
        }
        return correctedArray
    }
    
    //MARK: general helpers
    static let imageKey = "image"

 
}
